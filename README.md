# Server Template

This project is a template for server project for any arbitrary customer.

The goal of this template (and its applications) to have minimalistic server with very few dependencies without need
for any heavier dependencies like [Spring framework] or [Spring Boot]. So if the super simplicity is not a goal,
this template or its applications should very likely **NOT** be used.

This template is not just about the application itself but also about simple tooling that ease installation of the
applications to the operating system or providing [OpenAPI] API documentation.

Whole project is extensively documented. To read more about the structure of repository, refer to [README-project.md]
which may further lead to another documentation files.

## Use

To create a new copy of this project that will derive from this template with custom naming, follow these steps:

1. Run [Gradle wrapper] script `./gradlew --console=plain to-new-project` and go through wizard
2. Review and complete `README.md` files in newly created directory (search for `*!!` or `!!*`)

[Spring framework]: https://spring.io/

[Spring Boot]: https://spring.io/projects/spring-boot

[OpenAPI]: https://swagger.io/specification/

[README-project.md]: ./README-project.md

[Gradle]: https://gradle.org/

[Gradle wrapper]: https://docs.gradle.org/current/userguide/gradle_wrapper.html
