# Custom Server Name

This project contains server applications that provides HTTP REST-like APIs via embedded HTTP server with modular
architecture.

## Project Structure

All code related to the server applications is within [projects], and it further contains these key subdirectories:

- [apps]: applications (bundles) that can be built and delivered
- [core]: core logic of whole server logic (all modules depend on it)
- [modules]: modules that contribute to the whole server feature set (built on top of core)
- [http-servers]: implementations of embedded HTTP server (usually one is sufficient)
- [platform]: gradle [Java Platform Plugin] that mainly contains versions of used libraries in all projects

Most of these directories contain their own `README.md` files that describe more in detail purpose and implementation
details. To more understand final application bundles, see [custom-server-name] as a good example of application bundle.

## Build & Releasing

The whole project uses [Gradle] for a build using [Gradle wrapper] which takes care of downloading everything that is
needed for the build. To run unit tests and build applications' distribution ZIP files along with all scripts and
configuration files, run:

```sh
./gradlew clean test distZip
```

(or `./gradlew.bat` for Windows platform)

The output distribution ZIP archive can be found within `build/distributions` directory of each application (in [apps]).
If the tests fail, the whole build fails. Test report in HTML form can be found in `build/reports/tests` directory.

### New Version Checklist

Before releasing version of any application in [apps], make sure to go through this checklist:

1. Run all tests (on all dependent projects) and all must succeed
2. Add new version entry in `CHANGELOG.md` of given application
3. Update `version` in root (or application-specific) `build.gradle.kts`
4. Create and push a new Git tag `<appname>-version-x.y.z` (e.g. `custom-server-name-version-1.3.12`)

## Configuration

The configuration of applications is placed within `config` directory of each application and is well described in the
configuration files itself. Each module should have their `config-fragment-sample.yml` file within `assets` directory
if it requires any configuration. To generate `config.yml` for each application from those fragments, run this gradle
task (via wrapper) in root of this repository:

```shell
./gradlew generate-app-config
```

## OpenAPI Documentation

Each project or module that provides HTTP endpoints should have `<module-name>-api.yml` file within `assets`
subdirectory that is [OpenAPI] documentation of API. It is recommended to generate HTML alternative that is more
human-readable from those YAML files. For generation, it is required to have [ReDoc CLI], and then it is possible to run
this gradle task (via wrapper) in root of this repository:

```shell
./gradlew generate-openapi-html
```

It is also desirable to have documentation of all modules within the application `docs` directory. To copy documentation
from projects or modules to all applications that use it, run this gradle task (via wrapper) in root of this repository:

```shell
./gradlew copy-openapi-docs
```

## Libraries

Apart from main business-related libraries. Common JVM libraries with their dependencies are used:

- [Undertow] for embedded HTTP server
- [Jackson] for JSON serialization (output files)
- [SnakeYAML] for YAML deserialization (configuration file)
- [Logback] for logging

[projects]: ./projects

[apps]: ./projects/apps

[core]: ./projects/core

[modules]: ./projects/modules

[http-servers]: ./projects/http-servers

[platform]: ./projects/platform

[Java Platform Plugin]: https://docs.gradle.org/current/userguide/java_platform_plugin.html

[custom-server-name]: ./projects/apps/custom-server-name

[OpenAPI]: https://swagger.io/specification/

[Gradle]: https://gradle.org/

[Gradle wrapper]: https://docs.gradle.org/current/userguide/gradle_wrapper.html

[ReDoc CLI]: https://github.com/Redocly/redoc#redoc-cli

[Undertow]: http://undertow.io/

[Jackson]: https://github.com/FasterXML/jackson-dataformat-xml

[SnakeYAML]: https://bitbucket.org/asomov/snakeyaml/src/master/

[Logback]: http://logback.qos.ch/
