# Undertow HTTP Server

Implementation of HTTP server from core that uses [Undertow] for handling requests.
See core's [HttpServer] for more information about servers.

## Configuration

This server does not have any additional configuration.

[Undertow]: https://undertow.io/

[HttpServer]: ../../core/src/main/kotlin/online/zdenda/server/http/HttpServer.kt
