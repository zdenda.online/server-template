package online.zdenda.server.http

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import online.zdenda.server.auth.Auth
import online.zdenda.server.auth.AuthInfo
import online.zdenda.server.http.cfg.HttpServerCfg
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.handler.HttpHandlerAdapterTemplate
import java.nio.ByteBuffer

/**
 * Adapter of to Undertow's [io.undertow.server.HttpHandler] to core (adapted) [HttpHandler].
 * See [HttpHandlerAdapterTemplate] for more information how this adapter works.
 */
internal class UndertowHandlerAdapter(
    cfg: HttpServerCfg,
    adaptedHandler: HttpHandler,
    auth: Auth
) : HttpHandlerAdapterTemplate<HttpServerExchange>(cfg, adaptedHandler, auth), io.undertow.server.HttpHandler {

    override fun handleRequest(ex: HttpServerExchange) = adaptHandle(ex)

    override fun getRequestPath(ctx: HttpServerExchange): String {
        val params = ctx.queryParameters.map { qp ->
            "${qp.key} = ${qp.value.joinToString(",")}"
        }.joinToString(" | ")
        return "${ctx.requestURL} [$params]"
    }

    override fun getRequestHeader(ctx: HttpServerExchange, headerName: String): String? {
        return ctx.requestHeaders[headerName]?.first
    }

    override fun setResponseHeader(ctx: HttpServerExchange, headerName: String, headerValue: String) {
        ctx.responseHeaders.put(HttpString(headerName), headerValue)
    }

    override fun buildHttpRequest(ctx: HttpServerExchange, reqId: HttpRequestId, authInfo: AuthInfo): HttpRequest {
        val params = ctx.queryParameters.map { qp -> // Undertow includes path parameters in query parameters
            qp.key to qp.value.joinToString(HttpRequest.PARAMETER_VALUES_DELIMITER)
        }.toMap()
        val headers = ctx.requestHeaders.associate {
            it.headerName.toString() to it.first
        }
        val body = if (ctx.inputStream != null) ctx.inputStream.readBytes() else null
        return HttpRequest(
            reqId = reqId,
            authInfo = authInfo,
            method = HttpMethod.valueOf(ctx.requestMethod.toString()),
            parameters = params,
            headers = headers,
            body = body
        )
    }

    override fun sendResponse(ctx: HttpServerExchange, statusCode: Int, body: ByteArray) {
        ctx.statusCode = statusCode
        ctx.responseSender.send(ByteBuffer.wrap(body))
        ctx.responseSender.close()
    }
}
