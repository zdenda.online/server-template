package online.zdenda.server.http

import io.undertow.Handlers
import io.undertow.Undertow
import io.undertow.server.HttpHandler
import io.undertow.server.RoutingHandler
import io.undertow.server.handlers.BlockingHandler
import online.zdenda.server.auth.Auth
import online.zdenda.server.http.cfg.HttpServerCfg
import org.slf4j.LoggerFactory

/**
 * Implementation of [HttpServer] that uses [Undertow] for HTTP.
 *
 * It practically adapts Undertow API via [UndertowHandlerAdapter] to [HttpHandler].
 */
class UndertowHttpServer : HttpServer {

    private var undertow: Undertow? = null

    override fun start(
        cfg: HttpServerCfg,
        auth: Auth,
        handlers: List<online.zdenda.server.http.handler.HttpHandler>
    ) {
        undertow = Undertow.builder()
            .addHttpListener(cfg.port, "0.0.0.0")
            .setHandler(prepareRoutingHandler(cfg, auth, handlers))
            .build()
        try {
            undertow!!.start()
        } catch (e: RuntimeException) {
            logger.error("Unable to start HTTP server - ${e.message}", e)
        }
    }

    /**
     * Stops the server.
     */
    override fun stop() {
        if (undertow != null) undertow!!.stop()
    }

    private fun prepareRoutingHandler(
        cfg: HttpServerCfg,
        auth: Auth,
        handlers: List<online.zdenda.server.http.handler.HttpHandler>
    ): RoutingHandler {
        val routingHandler = Handlers.routing()
        handlers.forEach { handler ->
            val undertowHandler = BlockingHandler(UndertowHandlerAdapter(cfg, handler, auth))
            handler.methods.forEach { method ->
                routingHandler.add(method.name, handler.urlTemplate, undertowHandler)

            }
        }
        return routingHandler
    }


    companion object {
        private val logger = LoggerFactory.getLogger(UndertowHttpServer::class.java)
    }
}
