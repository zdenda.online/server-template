# Server Core

This project is the core for all modules, and it provides API that each module or contributor to the application
implement. Dependent modules simply add Gradle dependency to this project as follows: `implementation(project(":core"))`
and implement whatever feature they want to contribute to server.

Whole API is well-documented on the code level and thus does not require any additional description in here.
See existing modules (Gradle projects) that will guide you how to implement a new features.

The result of each project is usually a [Module] or in rare cases [HttpServer] implementation.

[Module]: src/main/kotlin/online/zdenda/server/Module.kt

[HttpServer]: src/main/kotlin/online/zdenda/server/http/HttpServer.kt
