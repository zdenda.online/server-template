package online.zdenda.server.http.handler

import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.response.HttpResponse

/**
 * Handler for HTTP OPTIONS pre-flight requests that just returns status 200 with empty body payload
 * (and CORS headers that are added by HTTP server).
 */
internal class OptionsHttpHandler : HttpHandler {

    override val methods = listOf(HttpMethod.OPTIONS)
    override val urlTemplate = "/*"
    override val authRoleName = null

    override fun handle(req: HttpRequest): HttpResponse = HttpResponse(200)
}
