package online.zdenda.server.http

import online.zdenda.server.auth.AuthInfo
import java.io.IOException

/**
 * HTTP request that can be handled via [HttpServer].
 */
class HttpRequest(
    /**
     * Unique ID of HTTP request
     */
    val reqId: HttpRequestId,
    /**
     * Authorization info parsed out of HTTP communication.
     */
    val authInfo: AuthInfo,
    /**
     * HTTP request method used for request.
     */
    val method: HttpMethod,
    /**
     * Parameters from HTTP request, either query parameters or path parameters.
     */
    val parameters: Map<String, String> = emptyMap(),
    /**
     * Headers of the request.
     */
    val headers: Map<String, String> = emptyMap(),
    /**
     * Body payload of the request (optional as body is not mandatory in HTTP requests).
     */
    val body: ByteArray? = null
) {

    /**
     * Gets a flag whether this request contains body (payload).
     */
    fun hasBody(): Boolean = (body != null)

    /**
     * Requires that this request has body (payload).
     * Otherwise [IllegalArgumentException] is thrown.
     */
    fun requireBody(): ByteArray {
        return body ?: throw IllegalArgumentException("Request body is required but is not present")
    }

    /**
     * Requires that this request has body (payload) that can be deserialized as JSON with given type.
     * Otherwise [IllegalArgumentException] is thrown.
     */
    inline fun <reified T> requireJsonBody(): T {
        try {
            return httpObjectMapper.readValue(requireBody(), T::class.java)
        } catch (e: IOException) {
            throw IllegalArgumentException("Invalid format of request body")
        }
    }

    /**
     * Requires value of specific parameter, otherwise [IllegalArgumentException] is thrown.
     *
     * @param name name of parameter
     */
    fun requireParameter(name: String): String {
        return parameters[name]
            ?: throw IllegalArgumentException("Request parameter '$name' is required but is missing")
    }

    /**
     * Gets multiple values in a single parameter.
     *
     * @param name name of parameter
     * @return values of parameter or empty list if no value is present
     */
    fun parameterValues(name: String): List<String> {
        return parameters[name]?.split(PARAMETER_VALUES_DELIMITER) ?: emptyList()
    }

    companion object {
        const val PARAMETER_VALUES_DELIMITER = ";"
    }
}
