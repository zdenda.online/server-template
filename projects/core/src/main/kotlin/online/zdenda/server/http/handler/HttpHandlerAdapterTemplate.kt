package online.zdenda.server.http.handler

import online.zdenda.server.auth.Auth
import online.zdenda.server.auth.AuthInfo
import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.HttpRequestId
import online.zdenda.server.http.HttpServer
import online.zdenda.server.http.cfg.HttpServerCfg
import online.zdenda.server.http.response.HttpResponse
import online.zdenda.server.http.response.MessageResponse
import online.zdenda.server.http.response.ServerErrorResponse
import org.slf4j.LoggerFactory

/**
 * Adapter for handling requests via adapted [HttpHandler]. It serves also as template method providing life-cycle of
 * the whole request handling and allows implementors just on doing necessary actions at correct time without knowing
 * the details.
 *
 * It is strongly recommended for [HttpServer] implementations to extend this class and use [adaptHandle] for handling
 * request.
 *
 * @param C generic context of the request specific to the implementation (typically in form of HTTP request context,
 * exchange or similar concepts) - this context will be passed to all template methods.
 */
abstract class HttpHandlerAdapterTemplate<C>(
    private val cfg: HttpServerCfg,
    private val adaptedHandler: HttpHandler,
    private val auth: Auth
) {

    /**
     * Gets HTTP request (URL) path. It is used for logging purposes, so is recommended that it contains query
     * parameters, but it is not necessary.
     *
     * @param ctx request context specific to implementation
     * @return HTTP request path
     */
    abstract fun getRequestPath(ctx: C): String

    /**
     * Gets value of HTTP request header.
     *
     * @param ctx request context specific to implementation
     * @param headerName name of header to be read
     * @return HTTP header value of given name or null if not present
     */
    abstract fun getRequestHeader(ctx: C, headerName: String): String?

    /**
     * Sets value of HTTP response header.
     *
     * @param ctx request context specific to implementation
     * @param headerName name of header to be set
     * @param headerValue value of header to be set
     */
    abstract fun setResponseHeader(ctx: C, headerName: String, headerValue: String)

    /**
     * Builds a [HttpRequest] instance out of context and provided parameters.
     * Implementations MUST use given [HttpRequestId] and [AuthInfo] in the result instance.
     *
     * @param ctx request context specific to implementation
     * @param reqId ID for the request
     * @param authInfo auth info for the request
     * @return HTTP request object
     */
    abstract fun buildHttpRequest(ctx: C, reqId: HttpRequestId, authInfo: AuthInfo): HttpRequest

    /**
     * Sends HTTP response and finishes processing of request by it (e.g. flushes output steam, closes connections...).
     *
     * @param ctx request context specific to implementation
     * @param statusCode status code to be sent
     * @param body body to be written as response body payload
     */
    abstract fun sendResponse(ctx: C, statusCode: Int, body: ByteArray)

    /**
     * Adapts request to [HttpHandler]. It takes care of whole request life-cycle so that implementation is mandated
     * just to provide implementation-specific context and call this method for HTTP request processing.
     *
     * @param ctx request context specific to implementation that will be passed in all template methods
     */
    fun adaptHandle(ctx: C) {
        val reqId = HttpRequestId.newId()
        logger.info("==> Accepted HTTP request on path '${getRequestPath(ctx)}' $reqId")
        addCorsResponseHeaders(ctx, reqId)

        val authInfo = authorize(ctx, reqId)
        if (adaptedHandler.authRoleName != null && !authInfo.isAuthorized) return // response already sent

        val request = buildHttpRequest(ctx, reqId, authInfo)
        handleAndSendResponse(ctx, reqId, request)
    }

    private fun authorize(ctx: C, reqId: HttpRequestId): AuthInfo {
        if (adaptedHandler.authRoleName == null) return AuthInfo.UNAUTHORIZED // no authorization requiredO

        val authHeader = getRequestHeader(ctx, "Authorization")

        if (authHeader == null) {
            logger.error("Authorization header is missing for request $reqId")
            logAndSendResponse(ctx, reqId, MessageResponse(reqId, "Authorization header is missing", 401))
            return AuthInfo.UNAUTHORIZED
        }

        val token = authHeader.substringAfter("Bearer ").trim()
        if (token.isEmpty()) {
            logger.error("No token in Authorization header $reqId")
            logAndSendResponse(ctx, reqId, MessageResponse(reqId, "No token in Authorization header", 401))
            return AuthInfo.UNAUTHORIZED
        }

        val authInfo = auth.asRole(roleName = adaptedHandler.authRoleName!!, token = token) // !! is safe
        if (!authInfo.isAuthorized) {
            logger.error("Token is not valid for role ${adaptedHandler.authRoleName} $reqId")
            logAndSendResponse(ctx, reqId, MessageResponse(reqId, "Invalid token", 403))
            return AuthInfo.UNAUTHORIZED
        }

        return authInfo
    }

    private fun handleAndSendResponse(ctx: C, reqId: HttpRequestId, request: HttpRequest) {
        try {
            val response = adaptedHandler.handle(request)
            logAndSendResponse(ctx, reqId, response)
        } catch (e: IllegalArgumentException) {
            logger.warn("Invalid request, reason: ${e.message}")
            logger.debug("Reason trace", e)
            logAndSendResponse(ctx, reqId, MessageResponse(reqId, e.message ?: "Validation failed", 400))
            return
        } catch (e: Exception) {
            logger.error("Internal server error", e)
            logAndSendResponse(ctx, reqId, ServerErrorResponse(reqId))
            return
        }
    }

    private fun addCorsResponseHeaders(ctx: C, reqId: HttpRequestId) {
        if (cfg.corsOrigins.isEmpty()) return // Not configured, do not add any origin

        val origin = getRequestHeader(ctx, "Origin")
        if (origin != null && cfg.corsOrigins.contains(origin)) {
            setResponseHeader(ctx, "Access-Control-Allow-Origin", origin)
            setResponseHeader(ctx, "Access-Control-Allow-Methods", HttpMethod.values().joinToString(", "))
            setResponseHeader(ctx, "Access-Control-Allow-Headers", "*")
            setResponseHeader(ctx, "Access-Control-Allow-Credentials", "true")
            setResponseHeader(ctx, "Access-Control-Allow-Max-Age", "86400")
        } else {
            logger.debug("Origin not sent or not allowed, no CORS headers will added to response $reqId")
        }
    }

    private fun logAndSendResponse(ctx: C, reqId: HttpRequestId, response: HttpResponse) {
        logger.info("<== Sent HTTP response (status=${response.statusCode}) of size ${response.body.size} bytes $reqId")
        if (!response.headers.map { it.key.lowercase() }.contains("Content-Length")) {
            setResponseHeader(ctx, "Content-Length", response.body.size.toString())
        }
        response.headers.forEach { setResponseHeader(ctx, it.key, it.value) }
        sendResponse(ctx, response.statusCode, response.body)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HttpHandlerAdapterTemplate::class.java)
    }
}
