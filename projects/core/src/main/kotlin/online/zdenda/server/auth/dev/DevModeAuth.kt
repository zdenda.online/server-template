package online.zdenda.server.auth.dev

import online.zdenda.server.auth.Auth
import online.zdenda.server.auth.AuthInfo
import online.zdenda.server.auth.dev.cfg.DevModeAuthCfg

/**
 * Auth to simplify development but MUST be disabled for production purposes.
 * It just serves configured user ID for given role.
 */
internal class DevModeAuth(
    private val cfg: DevModeAuthCfg
) : Auth {

    override fun asRole(roleName: String, token: String): AuthInfo {
        if (!cfg.enabled) throw IllegalStateException("Misconfiguration? Calling development mode auth that is disabled!")
        return cfg.roles
            .firstOrNull { it.roleName == roleName }
            ?.let { AuthInfo(it.userId) } ?: return AuthInfo.UNAUTHORIZED
    }
}
