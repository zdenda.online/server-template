package online.zdenda.server.http.response

import online.zdenda.server.http.HttpServer
import online.zdenda.server.http.handler.HttpHandler

/**
 * Represents a response of [HttpHandler] to be propagated back to client via [HttpServer].
 */
open class HttpResponse(
    /**
     * HTTP status code to be sent. Defaults to 200 OK status.
     */
    val statusCode: Int = 200,
    /**
     * HTTP headers to be sent. Defaults to empty map.
     */
    val headers: Map<String, String> = emptyMap(),
    /**
     * Body to be sent in response. Defaults to empty (0 bytes) body.
     */
    val body: ByteArray = ByteArray(0)
) {

    /**
     * Gets a flag whether response is a success, in other words status code is in range 200..299.
     */
    fun isSuccessful(): Boolean = (statusCode in 200..299)

    override fun toString(): String {
        val body = if (body.isEmpty()) "--empty body payload--" else body.toString(Charsets.UTF_8)
        return "Status: $statusCode\n${body}"
    }
}
