package online.zdenda.server.auth.dev.cfg

internal data class DevModeAuthCfg(
    val enabled: Boolean,
    val roles: List<DevModeRoleCfg>
) {

    companion object {
        private val DISABLED = DevModeAuthCfg(false, emptyList())

        fun load(cfg: Map<String, *>): DevModeAuthCfg {
            if (cfg["dev_mode_auth"] !is Map<*, *>) return DISABLED
            val devMode = cfg["dev_mode_auth"] as Map<*, *>

            val enabled = devMode["enabled"]?.toString()?.toBooleanStrict() ?: false
            if (!enabled) return DISABLED

            if (devMode["roles"] !is List<*>) DISABLED
            val authRoles = devMode["roles"] as List<*>

            val roles = authRoles.map { DevModeRoleCfg.load(it as Map<*, *>) }
            return DevModeAuthCfg(true, roles)
        }
    }
}
