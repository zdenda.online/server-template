package online.zdenda.server.task

import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.Timer
import java.util.TimerTask
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.scheduleAtFixedRate

/**
 * Repeated task implementation that uses Java API standard [Timer].
 */
class TimerRepeatedTask(
    /**
     * Name of repeated task for logging purposes.
     */
    private val actionName: String,
    /**
     * Initial delay before repeated task gets triggered for the first time.
     */
    private val initialDelay: Duration,
    /**
     * Polling delay between triggers (e.g. how long to wait before triggering again).
     */
    private val pollingDelay: Duration,
    /**
     * Action to be executed on each trigger.
     */
    private val action: () -> Unit,
) : RepeatedTask {

    private val atomicIsRunning = AtomicBoolean(false)
    private lateinit var pollingTask: TimerTask

    override val isRunning
        get() = atomicIsRunning.get()

    override fun start() {
        if (atomicIsRunning.get()) stop()

        pollingTask = Timer(actionName).scheduleAtFixedRate(initialDelay.toMillis(), pollingDelay.toMillis()) {
            trigger()
        }
    }

    override fun trigger(): Boolean {
        val wasNotRunning = atomicIsRunning.compareAndSet(false, true)
        if (wasNotRunning) {
            try {
                logger.info("Running repeated task '$actionName'")
                action.invoke()
                logger.info("Finished repeated task '$actionName'")
            } catch (e: Exception) { // do not let thread die in any circumstances
                logger.error("Error while executing repeated task", e)
            } finally {
                atomicIsRunning.set(false)
            }
        }
        return wasNotRunning
    }

    override fun stop() {
        if (atomicIsRunning.compareAndSet(true, false)) {
            logger.info("Stopping repeated task '$actionName'")
            pollingTask.cancel()
        } else {
            logger.warn("Polling task '$actionName' is not running, nothing to stop")
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(TimerRepeatedTask::class.java)
    }
}
