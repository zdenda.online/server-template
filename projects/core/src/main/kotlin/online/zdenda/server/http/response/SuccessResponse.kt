package online.zdenda.server.http.response

/**
 * Subtype of [HttpResponse] that can be used for signalization of successful or not successful operation.
 */
class SuccessResponse(
    success: Boolean,
    statusCode: Int = 200,
    headers: Map<String, String> = emptyMap(),
) : JsonHttpResponse(
    SuccessBody(success),
    statusCode,
    headers
) {
    class SuccessBody(val success: Boolean)

    companion object {
        val SUCCESS = SuccessResponse(success = true)
        val FAILED = SuccessResponse(success = false)
    }
}
