package online.zdenda.server.auth.dev.cfg

internal data class DevModeRoleCfg(
    val roleName: String,
    val userId: String
) {

    companion object {
        fun load(cfg: Map<*, *>): DevModeRoleCfg {
            return DevModeRoleCfg(
                roleName = cfg["role_name"].toString(),
                userId = cfg["user_id"].toString()
            )
        }
    }
}

