package online.zdenda.server.http

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonMapperBuilder
import java.math.BigDecimal


/**
 * Object mapper that should be used for HTTP serialization purposes to ensure consistency of returned types.
 * It serializes Java time API in standard date format and BigDecimal (typically for money amounts) as strings.
 */
val httpObjectMapper: ObjectMapper = jacksonMapperBuilder()
    .addModule(JavaTimeModule())
    .addModule(bigDecimalAsStringModule())
    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    .defaultDateFormat(StdDateFormat())
    .build()

private fun bigDecimalAsStringModule(): SimpleModule {
    val module = SimpleModule("BigDecimalAsString")
    module.addSerializer(BigDecimal::class.java, object : JsonSerializer<BigDecimal>() {
        override fun serialize(value: BigDecimal?, gen: JsonGenerator, serializers: SerializerProvider?) {
            if (value == null) {
                gen.writeNull()
            } else {
                gen.writeString(value.toPlainString())
            }
        }
    })
    return module
}
