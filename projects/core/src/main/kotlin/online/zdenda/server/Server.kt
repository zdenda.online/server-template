package online.zdenda.server

import ch.qos.logback.classic.util.ContextInitializer
import online.zdenda.server.Server.Companion.new
import online.zdenda.server.auth.Auth
import online.zdenda.server.auth.NoAuth
import online.zdenda.server.auth.dev.DevModeAuth
import online.zdenda.server.auth.dev.cfg.DevModeAuthCfg
import online.zdenda.server.http.HttpServer
import online.zdenda.server.http.cfg.HttpServerCfg
import online.zdenda.server.http.handler.OptionsHttpHandler
import online.zdenda.server.http.handler.RootHttpHandler
import online.zdenda.server.task.RepeatedTask
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.yaml.snakeyaml.Yaml
import java.io.Closeable
import java.io.File
import kotlin.system.exitProcess

/**
 * Main core class that application modules use.
 * It takes care of the server life-cycle by doing all operations among registered [Module] instances using given
 * [HttpServer].
 *
 * Typical use is to instantiate this class with desired [HttpServer] via [new], provide [HttpServer] implementation
 * with [withHttpServer] and then all wanted modules via subsequent [withModule] and finally starting the server via
 * [start].
 *
 * See [Module] and [HttpServer] for more information about their details.
 */
class Server private constructor(
    private val cfg: Map<String, *>,
    private val logger: Logger
) {

    /**
     * This must be called first, otherwise other classes (e.g. HTTP handler or modules) initialize logging and
     * similar sub-systems first and their configuration will be ignored.
     */
    companion object {

        /**
         * Creates a new server instance from provided CLI arguments.
         * This method must be fist method of main method before initialization of any other component.
         * Otherwise, it may happen that some sub-systems (e.g. logging) is not loaded properly.
         */
        fun new(cliArgs: Array<String>): Server {
            printIntro()
            val configDir = parseConfigDirFromArgs(cliArgs)
            val cfg = loadConfiguration(configDir)
            val logger = LoggerFactory.getLogger("Server")
            printIntro(logger) // print art in logs as well, it is easier to find out when application was started
            return Server(cfg, logger)
        }

        private fun printIntro(logger: Logger? = null) {
            val art = Server::javaClass.javaClass.getResource("/ascii-art.txt")?.readText(Charsets.UTF_8)
            if (logger == null) {
                if (art != null) println(art)
            } else {
                if (art != null) logger.info(art)
            }
        }

        private fun parseConfigDirFromArgs(args: Array<String>): File {
            val configDir: File = when {
                args.isEmpty() -> File("../config")
                args.size == 1 -> File(args[0])
                else -> {
                    println("Application requires zero or one argument that is path to valid configuration directory")
                    exitProcess(10)
                }
            }

            if (!configDir.exists() || !configDir.canRead()) {
                println("Configuration directory '${configDir.canonicalPath}' is not valid directory (with read permissions)")
                exitProcess(11)
            }

            val configFile = File(configDir, "config.yml")
            val logbackFile = File(configDir, "logback.xml")
            if (!configFile.exists() || !configFile.canRead() || !logbackFile.exists() || !logbackFile.canRead()) {
                println("Application configuration directory '${configDir.canonicalPath}' does not contain readable config.yml and logback.xml")
                exitProcess(12)
            }

            return configDir
        }

        private fun loadConfiguration(configDir: File): Map<String, *> {
            val localConfigYml = File(configDir, "config-local.yml") // try local config file first
            val configFile = if (localConfigYml.exists()) localConfigYml else File(configDir, "config.yml")
            val logbackConfig = File(configDir, "logback.xml")

            System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, logbackConfig.canonicalPath)
            try {
                return Yaml().load(configFile.readText(Charsets.UTF_8))
            } catch (e: Exception) {
                println(e.message)
                exitProcess(15)
            }
        }
    }

    private val modules = linkedSetOf<Module>()
    private lateinit var httpServer: HttpServer

    /**
     * Sets embedded HTTP server for this server.
     *
     * @param httpServer HTTP server implementation to be used
     * @return this instance
     */
    fun withHttpServer(httpServer: HttpServer): Server = apply { this.httpServer = httpServer }


    /**
     * Adds a module to be used by the server.
     *
     * @param module module to be used
     * @return this instance
     */
    fun withModule(module: Module): Server = apply { modules.add(module) }

    /**
     * Starts the server with given embedded HTTP server and modules.
     */
    fun start() {
        if (isMisconfigured()) return
        try {
            loadModules()
            val auth = loadAuth()
            val repeatedTasks = loadRepeatedTasks()
            addShutdownHook(repeatedTasks)
            startHttpServer(auth)
        } catch (e: IllegalArgumentException) {
            println(e.message)
            logger.error("Unable to start server: ${e.message}")
            exitProcess(21)
        }
    }

    private fun isMisconfigured(): Boolean {
        if (!::httpServer.isInitialized) {
            val msg = "No HTTP server was provided, did you forget to call 'withHttpServer'?"
            logger.error(msg)
            println(msg)
            exitProcess(20)
        }

        println("Application is starting, see logs for further details (according logback.xml)...")
        return false
    }


    private fun loadModules() {
        logger.info("Loading modules...")
        val modules = modules.map { module ->
            logger.info("Loading module '${module.name}'...")
            module.load(cfg)
            module
        }
        logger.info("Loaded ${modules.size} modules successfully")
    }

    private fun loadAuth(): Auth {
        logger.info("Resolving auth...")
        val devModeAuthCfg = DevModeAuthCfg.load(cfg)
        if (devModeAuthCfg.enabled) {
            logger.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            logger.warn("!!! USING DEVELOPMENT MODE AUTH - YOU NEVER SHOULD SEE THIS MESSAGE IN PRODUCTION !!!")
            logger.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            return DevModeAuth(devModeAuthCfg)
        }

        val availableAuths = modules.mapNotNull { it.auth(cfg) }
        return when (availableAuths.size) {
            0 -> {
                logger.info("No auth registered by any module and development mode is disabled, only public resources will be available")
                NoAuth()
            }

            1 -> {
                val auth = availableAuths.first()
                logger.info("Using auth '${auth.javaClass.simpleName}'")
                auth
            }

            else -> throw IllegalArgumentException("More than one candidate for auth: " +
                    availableAuths.joinToString(", ") { "'${it.javaClass.simpleName}'" }
            )
        }
    }

    private fun loadRepeatedTasks(): List<RepeatedTask> {
        logger.info("Loading repeated tasks...")
        val tasks = modules.map { it.repeatedTasks(cfg) }.flatten()
        tasks.forEach { task ->
            logger.info("Starting repeating task '${task.javaClass.simpleName}'")
            task.start()
        }
        logger.info("Loaded and started ${tasks.size} repeated tasks successfully")
        return tasks
    }

    private fun addShutdownHook(repeatedTasks: List<RepeatedTask>) {
        val closeables: MutableList<Closeable> = repeatedTasks.toMutableList()
        modules.forEach { closeables.addAll(it.closeables()) }
        closeables.add(httpServer) // add HTTP server as the last closeable as it runs main thread

        Runtime.getRuntime().addShutdownHook(Thread {
            closeables.distinct().forEach {
                logger.info("Closing '${it.javaClass.simpleName}'...")
                it.close()
            }
        })
        logger.info("Registered ${closeables.size} closeable instances to shutdown hook")
    }

    private fun startHttpServer(auth: Auth) {
        logger.info("Using HTTP server '${httpServer.javaClass.simpleName}'")
        val serverCfg = HttpServerCfg.load(cfg)
        logger.info("Loading HTTP handlers...")
        val handlers = linkedSetOf(RootHttpHandler(), OptionsHttpHandler())
        modules.forEach { handlers.addAll(it.httpHandlers(cfg)) }
        handlers.forEach { logger.info("Loaded HTTP handler '${it.javaClass.simpleName}'") }
        logger.info("Starting HTTP server with ${handlers.size} handlers, will listen on port ${serverCfg.port}...")
        httpServer.start(serverCfg, auth, handlers.toList())
    }
}
