package online.zdenda.server.http

import online.zdenda.server.http.response.HttpResponse
import java.io.FileNotFoundException
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

/**
 * Encodes this string via URL encoding.
 * This is useful for making requests with www-form-url-encoded body requests.
 */
fun String.urlEncode(): String {
    return URLEncoder.encode(this, "utf-8")
}

/**
 * Executes HTTP GET request treating this String as URL providing the result.
 *
 * @param headers headers to be added to the request
 * @return byte response of null if request fails
 */
fun String.httpGet(headers: Map<String, String> = emptyMap()): HttpResponse =
    this.executeRequest("GET", null, headers)

/**
 * Executes HTTP POST request treating this String as URL using given body and headers in the request.
 *
 * @param body body to be sent as body payload
 * @param headers headers to be added to the request
 * @return byte response of null if request fails
 */
fun String.httpPost(body: ByteArray, headers: Map<String, String> = emptyMap()): HttpResponse =
    this.executeRequest("POST", body, headers)

private fun String.executeRequest(
    method: String,
    body: ByteArray? = null,
    headers: Map<String, String> = emptyMap()
): HttpResponse {
    val urlObj = URL(this)
    return with(urlObj.openConnection() as HttpURLConnection) {
        doInput = true
        doOutput = (body != null)
        requestMethod = method
        headers.forEach { h -> addRequestProperty(h.key, h.value) }

        var resolvedResponseCode = -1
        var respBody = ByteArray(0)
        var responseHeaders: Map<String, String> = emptyMap()
        try {
            if (body != null) {
                outputStream.write(body)
                outputStream.flush()
            }
            resolvedResponseCode = responseCode // actually may throw exception
            responseHeaders = headerFields
                .map { it.key to it.value.joinToString(",") }
                .toMap()
            respBody = inputStream?.readBytes() ?: respBody
        } catch (e: FileNotFoundException) { // funny JVM API throws this on 404
            try {
                respBody = errorStream?.readBytes() ?: respBody
            } catch (_: IOException) {
                // ¯\_(ツ)_/¯
            }
        } catch (e: IOException) {
            try {
                respBody = errorStream?.readBytes() ?: respBody
            } catch (_: IOException) {
                // ¯\_(ツ)_/¯
            }
        }
        HttpResponse(resolvedResponseCode, responseHeaders, respBody)
    }
}
