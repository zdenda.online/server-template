package online.zdenda.server.http.handler

import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.response.HttpResponse
import online.zdenda.server.http.response.SuccessResponse
import online.zdenda.server.task.RepeatedTask
import org.slf4j.LoggerFactory

/**
 * Simple abstract handler that just triggers given repeated tasks and returns whether it was triggered or not.
 */
abstract class TriggerTaskHttpHandler(
    private val repeatedTask: RepeatedTask
) : HttpHandler {

    override val methods: List<HttpMethod> = listOf(HttpMethod.POST)

    override fun handle(req: HttpRequest): HttpResponse {
        logger.info("Triggering repeated task '${this.javaClass.simpleName}' ${req.reqId}")
        val triggered = repeatedTask.trigger()
        logger.info("Repeated task '${this.javaClass.simpleName}' was ${if (triggered) "successfully" else "NOT"} triggered")
        return SuccessResponse(triggered)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(TriggerTaskHttpHandler::class.java)
    }
}
