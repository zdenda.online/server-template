package online.zdenda.server.http.response

import online.zdenda.server.http.HttpRequestId

/**
 * Subtype of [MessageResponse] that is suited for unexpected server error messages.
 */
class ServerErrorResponse(
    reqId: HttpRequestId,
) : MessageResponse(
    statusCode = 500,
    reqId = reqId,
    message = "Unexpected error occurred, please contact administrator with request ID"
)
