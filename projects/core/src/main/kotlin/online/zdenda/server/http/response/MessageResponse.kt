package online.zdenda.server.http.response

import online.zdenda.server.http.HttpRequestId

/**
 * Subtype of [HttpResponse] that can be used for arbitrary messages.
 */
open class MessageResponse(
    reqId: HttpRequestId,
    message: String,
    statusCode: Int = 200,
    headers: Map<String, String> = emptyMap()
) : JsonHttpResponse(
    MessageBody(message, reqId.id),
    statusCode,
    headers
) {

    class MessageBody(val message: String, val requestId: String)
}
