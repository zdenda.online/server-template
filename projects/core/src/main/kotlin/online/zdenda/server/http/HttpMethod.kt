package online.zdenda.server.http

/**
 * Simple enumeration for type-safe HTTP methods.
 */
enum class HttpMethod {

    GET, POST, PUT, DELETE, PATCH, TRACE, OPTIONS, HEAD;
}
