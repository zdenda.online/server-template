package online.zdenda.server.http.response

import online.zdenda.server.http.httpObjectMapper

/**
 * Subtype of [HttpResponse] that provides body as serialized JSON along with application/json content type header.
 */
open class JsonHttpResponse(
    body: Any,
    statusCode: Int = 200,
    headers: Map<String, String> = emptyMap(),
) : HttpResponse(
    statusCode,
    headers.toMutableMap().apply { put("Content-Type", "application/json") },
    httpObjectMapper.writeValueAsBytes(body)
)
