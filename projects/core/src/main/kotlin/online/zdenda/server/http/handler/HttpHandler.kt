package online.zdenda.server.http.handler

import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.HttpServer
import online.zdenda.server.http.response.HttpResponse

/**
 * HTTP handler for handling API requests.
 */
interface HttpHandler {

    /**
     * HTTP methods to be used for this handler.
     */
    val methods: List<HttpMethod>

    /**
     * Template which URLs this handler should cover.
     */
    val urlTemplate: String

    /**
     * Name of role that should be used for authorizing this handler.
     * Can return null and in that case, the endpoint is public (without auth).
     */
    val authRoleName: String?

    /**
     * Handles the request providing generic response that [HttpServer] is mandated to write.
     *
     * Handlers can throw [IllegalArgumentException] that will be treated as invalid request (with 400 HTTP status).
     * Any other exception will be treated as server internal error (with 500 HTTP status).
     *
     * @param req request to be processed.
     * @throws IllegalArgumentException possible exception if provided request is not valid
     */
    fun handle(req: HttpRequest): HttpResponse
}
