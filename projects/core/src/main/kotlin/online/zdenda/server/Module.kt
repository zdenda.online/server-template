package online.zdenda.server

import online.zdenda.server.auth.Auth
import online.zdenda.server.auth.dev.DevModeAuth
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.task.RepeatedTask
import java.io.Closeable

/**
 * Represents a module that can be loaded by the server at the start-up.
 * The life-cycle of module goes in these phases (none is mandatory, module can hook to any of those phases):
 *
 * - [load] = first action where module can typically prepare any resources or trigger custom actions
 * - [auth] = if module implements any [Auth], it can provide it for other modules via this method
 * - [httpHandlers] = if module provides any [HttpHandler] instances, it can provide them to be registered to HTTP server
 * - [repeatedTasks] = if module has any [RepeatedTask] instances, this provides them and starts them
 * - [closeables] = adds any module custom [Closeable] instances to be closed on server stop
 *
 * In cases when any of those phases fail (e.g. due to misconfiguration), module can throw [IllegalArgumentException].
 */
interface Module {

    /**
     * Human-readable name (will be used for logging purposes).
     */
    val name: String

    /**
     * Loads the module with provided configuration.
     *
     * @param cfg configuration
     * @throws IllegalArgumentException exception in case module cannot be loaded
     */
    fun load(cfg: Map<String, *>) {}

    /**
     * Module can contribute to authorization via returned implementation.
     * In case of development mode, this authorization will be ignored and [DevModeAuth] will be used instead.
     *
     * @param cfg configuration
     * @throws IllegalArgumentException exception in case auth cannot be provided
     */
    fun auth(cfg: Map<String, *>): Auth? = null

    /**
     * Module can contribute HTTP API via handlers that this method returns.
     * It is recommended to provide them from most specific to most generic
     * in terms of URL template ([HttpHandler.urlTemplate]).
     *
     * @param cfg configuration
     * @throws IllegalArgumentException exception in case handlers cannot be provided
     */
    fun httpHandlers(cfg: Map<String, *>): List<HttpHandler> = emptyList()

    /**
     * Module can contribute with repeated tasks that will start on server run and get stopped when server stops
     * via shutdown hook.
     *
     * @param cfg configuration
     * @throws IllegalArgumentException exception in case repeated tasks cannot be provided
     */
    fun repeatedTasks(cfg: Map<String, *>): List<RepeatedTask> = emptyList()

    /**
     * Provides list of resources that are closeable and will be closed on server stop during shutdown hook.
     * Implementation is optional and defaults to empty list.
     *
     * It is not needed to provide [repeatedTasks] by this method as these, but it serves for any other [Closeable].
     *
     * @throws IllegalArgumentException exception in case closeables cannot be provided
     */
    fun closeables(): List<Closeable> = emptyList()
}
