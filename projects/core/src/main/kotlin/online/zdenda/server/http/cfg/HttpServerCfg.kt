package online.zdenda.server.http.cfg

/**
 * Configuration of HTTP server.
 */
data class HttpServerCfg(
    /**
     * Port of HTTP server.
     */
    val port: Int,
    /**
     * Allowed CORS origins.
     */
    val corsOrigins: List<String>,
) {
    companion object {

        fun load(cfg: Map<String, *>): HttpServerCfg {
            val httpServer = cfg["http_server"] as Map<*, *>
            return HttpServerCfg(
                port = httpServer["port"].toString().toInt(),
                corsOrigins = (httpServer["cors_origins"] as List<*>).map { it.toString() },
            )
        }
    }
}
