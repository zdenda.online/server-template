package online.zdenda.server.http.handler

import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.response.HttpResponse

/**
 * Handler for HTTP GET root path request that just returns OK message that server is UP and running
 * (and CORS headers that are added by HTTP server).
 */
internal class RootHttpHandler : HttpHandler {

    override val methods = listOf(HttpMethod.OPTIONS)
    override val urlTemplate = "/*"
    override val authRoleName = null

    override fun handle(req: HttpRequest): HttpResponse = HttpResponse(200)
}
