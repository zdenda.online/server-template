package online.zdenda.server.task

import java.io.Closeable

/**
 * Repeated task that periodically executes given action.
 *
 * Simply [start] to start the repeating of task (action) or [stop] (or [close]).
 * Eventually it can be triggered one-time via [trigger].
 */
interface RepeatedTask : Closeable {

    /**
     * Gets a flag whether this task is running.
     */
    val isRunning: Boolean

    /**
     * Starts repeating action to trigger.
     * Note that standard implementation of [TimerRepeatedTask] uses initial delay so that this start may not trigger
     * right away but after some defined delay.
     */
    fun start()

    /**
     * Triggers action one-time (if is not currently running).
     *
     * @return true if action was triggered, false if not as it was already in progress (and thus not triggered)
     */
    fun trigger(): Boolean

    /**
     * Stops repeated task.
     */
    fun stop()

    /**
     * Closes repeated task by stopping it.
     */
    override fun close() = stop()
}
