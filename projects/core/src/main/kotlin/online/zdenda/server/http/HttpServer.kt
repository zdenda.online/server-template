package online.zdenda.server.http

import online.zdenda.server.auth.Auth
import online.zdenda.server.http.cfg.HttpServerCfg
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.handler.HttpHandlerAdapterTemplate
import java.io.Closeable

/**
 * Embedded HTTP server that is responsible for handling incoming requests via [HttpHandler] implementations.
 * It is strongly recommended to use [HttpHandlerAdapterTemplate] and its [HttpHandlerAdapterTemplate.adaptHandle] that
 * takes care of proper life-cycle of the request and ensures equal behavior no matter the implementation details.
 *
 * The [HttpHandlerAdapterTemplate] is template method that allows HTTP server to focus only on providing correct
 * actions from the implementation-specific context and not taking care of whole request life-cycle. If the template
 * method is not used, the HTTP server must check authorization of request via provided [Auth] based on
 * [HttpHandler.authRoleName] and add CORS headers if request origin matches one of those in configuration.
 */
interface HttpServer : Closeable {

    /**
     * Starts the server with given configuration and handlers.
     *
     * @param cfg configuration of HTTP server to be used
     * @param auth authorization to be used by server
     * @param handlers handlers to be used for handling HTTP requests.
     */
    fun start(cfg: HttpServerCfg, auth: Auth, handlers: List<HttpHandler>)

    /**
     * Stops the server.
     */
    fun stop()

    /**
     * Closes the server by stopping it.
     */
    override fun close() = stop()
}
