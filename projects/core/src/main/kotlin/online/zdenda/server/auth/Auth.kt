package online.zdenda.server.auth

/**
 * Auth of users depending on their role (customers, operators...).
 */
interface Auth {

    /**
     * Authorizes as given role via provided token.
     *
     * @param roleName name of role to be authorized as
     * @param token token to be used for authorization
     * @return result info
     */
    fun asRole(roleName: String, token: String): AuthInfo
}
