package online.zdenda.server.auth

/**
 * Auth info, typically parsed out of request.
 */
data class AuthInfo(
    /**
     * ID of user (typically 'sub' claim from token)
     */
    val userId: String,
) {

    val isAuthorized: Boolean = userId.isNotBlank()

    companion object {
        val UNAUTHORIZED = AuthInfo("")
    }
}
