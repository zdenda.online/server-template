package online.zdenda.server.auth

/**
 * Implementation of auth that throws exception any time when called and is used as fallback check if no module
 * provided any auth and development mode is disabled.
 */
class NoAuth : Auth {
    override fun asRole(roleName: String, token: String): AuthInfo =
        throw RuntimeException("Attempting to authorize as $roleName but no auth was configured")
}
