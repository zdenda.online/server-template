package online.zdenda.server.http

import java.util.UUID

/**
 * Unique identifier of HTTP request (typically for logging purposes).
 */
@JvmInline
value class HttpRequestId(val id: String) {

    override fun toString(): String = "(requestId=${id})"

    companion object {
        fun newId(): HttpRequestId = HttpRequestId(UUID.randomUUID().toString())
    }
}
