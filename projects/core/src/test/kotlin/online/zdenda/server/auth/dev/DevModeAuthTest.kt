package online.zdenda.server.auth.dev

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import online.zdenda.server.auth.AuthInfo
import online.zdenda.server.auth.dev.cfg.DevModeAuthCfg
import online.zdenda.server.auth.dev.cfg.DevModeRoleCfg

/**
 * Ignore visibility error by IDE - it is possibly IDE bug as internal class is visible in test
 */
class DevModeAuthTest : BehaviorSpec({

    given("disabled development mode") {
        val auth = DevModeAuth(DevModeAuthCfg(false, emptyList()))

        `when`("authorizing as any role and any token") {

            then("should throw IllegalStateException") {
                shouldThrow<IllegalStateException> { auth.asRole(roleName = "operator", token = "doesnt-matter") }
            }
        }
    }

    given("development mode auth with no roles configured") {
        val auth = DevModeAuth(DevModeAuthCfg(true, emptyList()))

        `when`("authorizing as any role and any token") {
            val result = auth.asRole(roleName = "operator", token = "doesnt-matter")

            then("result is unauthorized") {
                result shouldBe AuthInfo.UNAUTHORIZED
                result.isAuthorized shouldBe false
            }
        }
    }

    given("development mode auth with operator role configured") {
        val auth = DevModeAuth(
            DevModeAuthCfg(
                true,
                listOf(
                    DevModeRoleCfg("operator", "test-id")
                )
            )
        )

        `when`("authorizing as operator and any token") {
            val result = auth.asRole(roleName = "operator", token = "doesnt-matter")

            then("result is authorized with configured test-id") {
                result shouldBe AuthInfo("test-id")
                result.isAuthorized shouldBe true
            }
        }

        `when`("authorizing as customer and any token") {
            val result = auth.asRole(roleName = "customer", token = "doesnt-matter")

            then("result is unauthorized") {
                result shouldBe AuthInfo.UNAUTHORIZED
                result.isAuthorized shouldBe false
            }
        }
    }
})
