dependencies {
    implementation("org.yaml:snakeyaml")
    api("ch.qos.logback:logback-classic")
    api("org.slf4j:log4j-over-slf4j") // ignore missing Log4J appender

    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    testImplementation("io.kotest:kotest-runner-junit5-jvm")
    testImplementation("io.kotest:kotest-assertions-core-jvm")
    testImplementation("io.kotest:kotest-property-jvm")
}
