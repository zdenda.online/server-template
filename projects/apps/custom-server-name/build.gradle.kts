plugins {
    application
}

dependencies {
    implementation(project(":core"))
    implementation(project(":undertow-http-server"))
    implementation(project(":oidc-auth-module"))
    implementation(project(":emails-module"))
    implementation(project(":proxy-module"))
    implementation(project(":fs-assets-storage-module"))
}

application {
    mainClass.set("online.zdenda.server.app.AppKt")
}

distributions {
    main {
        contents {
            from("README.md")
            from("CHANGELOG.md")
            into("install/") { from("install/") }
            into("config/") {
                from("config/")
                    .exclude("config-local.yml", "*.log")
            }
            into("docs/") { from("docs/") }
        }
    }
}

