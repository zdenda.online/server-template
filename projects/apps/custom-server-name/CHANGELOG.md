# Changelog

All notable changes in this application will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic Versioning].

## 0.0.1 - yyyy-mm-dd

### Added

- Initial version of the application

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/

[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
