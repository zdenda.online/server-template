package online.zdenda.server.app

import online.zdenda.server.Server
import online.zdenda.server.assets.FileSystemAssetsStorageModule
import online.zdenda.server.auth.oidc.OidcAuthModule
import online.zdenda.server.email.EmailsModule
import online.zdenda.server.email.ProxyModule
import online.zdenda.server.http.UndertowHttpServer

fun main(args: Array<String>) {
    Server.new(args)
        .withHttpServer(UndertowHttpServer())
        .withModule(OidcAuthModule())
        .withModule(EmailsModule())
        .withModule(ProxyModule())
        .withModule(FileSystemAssetsStorageModule())
        .start()
}
