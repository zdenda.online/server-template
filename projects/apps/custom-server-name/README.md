# Custom Server Name

## About Application

*!! Write few words here about primary use-cases this application solves. !!*

## Prerequisites

*!! Replace or add contents to this section about what prerequisites are required for application run !!*

The application is standalone JVM application that requires **Java 17+** (plain JRE for run or JDK for build, can be
both Oracle or OpenJDK).

## Installation

*!! Replace or add contents to this section about what is needed to perform during application installation !!*

This application has single executable and is recommended to be installed as a [systemd] service while it resides in
`/opt` directory as recommended according to [opt in Filesystem Hierarchy Standard].

If this is the first time the application is installed, run [install-first-version.sh] in `install` directory.
If only a new version is installed (and application was installed already), run [install-new-version.sh] instead.

The script should take care of copying files and creating OS group + user and systemd service if needed (during first
install).

## Configuration

The configuration of application is placed within `config` directory and consists of two configuration files:

- `config.yml`: application configuration
- `logback.xml`: logging configuration

Both configuration files are present in the distribution. Always review their contents (and consult this readme) before
running the application.

In case of development, developers may create `config-local.yml` file in `config` directory and such file will be
prioritized over standard `config.yml`.

### Application Configuration

The application configuration is documented directly within its `config.yml` file.

### Logging Configuration

Logging configuration is present in `logback.xml` file, and usually it is sufficient to just review property `logsDir`
at the beginning of XML file. This property defines a directory where log files will be generated, and it usually should
be a directory that suits used OS or company logging practices.

Other parts of configuration usually do not need modifications, they are usually changed in case of bug or performance
investigations. The most common approach for debugging is setting specific logger(s) to lower level of `DEBUG` or even
`TRACE`. For more information about Logback configuration, consult [Logback documentation].

By default, the logs are put only in log files but if needed, it is possible to un-comment `ASYNC-STDOUT` appender that
logs also to the standard output of OS.

## Run

The application should be ideally run as [systemd] service as described in Installation section above. To start it, run:

```shell
systemctl start custom-server-name
```

If system service is not desired, application can be started directly by executing `custom-server-name` script (`.bat`
for Windows platform) from `bin` directory:

```shell
cd bin
./custom-server-name
```

The application run script accepts zero or one argument pointing to the directory where configuration files are present.
If no argument is passed, it uses `../config` as default. That implies working directory must within `bin` directory in
case of no-argument execution (so that default `../` works).

### API Documentation

All endpoints that this application provides are documented via [OpenAPI] and can be found within [docs] directory in
original `yml` or derived (human-readable) `html` formats.

## Author & License

*!! Replace or add contents to this section about who is the author and what is application licensing model !!*

The application was designed and implemented by [--developer--] for [--customer--] company.
No use or redistribution of application parts specific to [--customer--] is allowed without company permission.

**[--customer--] is responsible for testing and production (LIVE) deployment decision**. Production (LIVE) deployment
means publishing application to environment where it will work with real data of company clients. This deployment should
be made after serious end-to-end testing made by [--customer--]. The author is not paid for this kind of testing. By
deploying the application to the production, [--customer--] effectively accepts the application and confirms its
correctness. *The author is not responsible for any (financial or non-financial) losses related to the use of
application after this deployment.*

[systemd]: https://systemd.io/

[opt in Filesystem Hierarchy Standard]: https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03s13.html

[install-first-version.sh]: ./install/install-first-version.sh

[install-new-version.sh]: ./install/install-new-version.sh

[Logback documentation]: http://logback.qos.ch/manual/index.html

[OpenAPI]: https://swagger.io/specification/

[docs]: ./docs

[--developer--]: https://developer-webpage

[--customer--]: https://customer-webpage
