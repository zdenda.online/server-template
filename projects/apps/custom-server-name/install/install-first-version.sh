#!/bin/bash

# Prepare variables pointing to correct directories
target_app_dir="/opt/custom-server-name"
install_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
app_dir=$(
  cd "$install_dir/.." || exit
  pwd
)
app_dir_name=$(basename "$app_dir")

# Copy application to target directory and create symlink for current
echo "Copying application '$app_dir_name' to '$target_app_dir'"
mkdir -p "$target_app_dir"/versions
cp -R "$app_dir" "$target_app_dir"/versions
ln -s "$target_app_dir"/versions/"$app_dir_name" "$target_app_dir"/current

# Making scripts executable
echo "Adding +x to scripts in '$target_app_dir/current'"
chmod +x "$target_app_dir"/current/bin/*

# Create user with home to application directory and make him owner there
os_user="custom-server-name"
echo "Creating '$os_user' user and group for application"
groupadd "$os_user"
useradd -r -g "$os_user" -d "$target_app_dir" "$os_user"
chown -R "$os_user":"$os_user" "$target_app_dir"

# Create storage for data on file-system
echo "Creating storage for data in '/srv/custom-server-name-storage"
mkdir -p "/srv/custom-server-name-storage"
chown -R "$os_user":"$os_user" "/custom-server-name-storage"

# Prepare systemd service and enable it
service_name="custom-server-name"
echo "Preparing and enabling '$service_name' systemd service"
cp "$install_dir/server.service" "/etc/systemd/system/$service_name.service"
systemctl daemon-reload
systemctl enable custom-server-name
