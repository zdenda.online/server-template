#!/bin/bash

# Prepare variables pointing to correct directories
target_app_dir="/opt/custom-server-name"
install_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
app_dir=$(
  cd "$install_dir/.." || exit
  pwd
)
app_dir_name=$(basename "$app_dir")

# Stop systemd server service
systemctl stop custom-server-name

# Backup old config files
tmp_config_dir="/tmp/custom-server-name-config-backup"
mkdir -p "$tmp_config_dir"
rm -f "$tmp_config_dir"/*
cp "$target_app_dir"/current/config/* "$tmp_config_dir"/

# Copy application to target directory and create symlink for current
echo "Copying application '$app_dir_name' to '$target_app_dir/versions'"
mkdir -p "$target_app_dir"/versions
cp -R "$app_dir" "$target_app_dir"/versions
rm "$target_app_dir"/current
ln -s "$target_app_dir"/versions/"$app_dir_name" "$target_app_dir"/current

# Copy old config files to new installation (to separate directory)
prev_version_config_dir="$target_app_dir"/current/config/previous-version
mkdir -p "$prev_version_config_dir"
cp "$tmp_config_dir"/* "$prev_version_config_dir"

# Making scripts executable
echo "Adding +x to scripts in '$target_app_dir/current'"
chmod +x "$target_app_dir"/current/bin/*

# Change owner of new version directory to app user
os_user="custom-server-name"
echo "Changing ownership of new version to '$os_user' user and group"
chown -R "$os_user":"$os_user" "$target_app_dir"
