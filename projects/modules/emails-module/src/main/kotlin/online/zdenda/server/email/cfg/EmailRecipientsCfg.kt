package online.zdenda.server.email.cfg

/**
 * Represents a multiple recipients of an e-mail defined by the single code.
 */
internal data class EmailRecipientsCfg(
    val code: String,
    val addresses: List<String>
) {

    companion object {
        fun load(cfg: Map<*, *>): EmailRecipientsCfg {
            return EmailRecipientsCfg(
                code = cfg["code"].toString(),
                addresses = (cfg["addresses"] as List<*>).map { it.toString() }
            )
        }
    }
}
