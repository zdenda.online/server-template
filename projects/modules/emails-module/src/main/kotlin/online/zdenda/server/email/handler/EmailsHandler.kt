package online.zdenda.server.email.handler

import online.zdenda.server.email.domain.EmailRequestBody
import online.zdenda.server.email.domain.EmailServer
import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.response.HttpResponse
import online.zdenda.server.http.response.ServerErrorResponse
import online.zdenda.server.http.response.SuccessResponse

/**
 * Handler that just maps incoming HTTP request to [EmailServer.sendEmail] domain call.
 */
internal class EmailsHandler(
    private val emailsServer: EmailServer
) : HttpHandler {

    override val methods = listOf(HttpMethod.POST)
    override val urlTemplate = "/public/messages"
    override val authRoleName = null

    override fun handle(req: HttpRequest): HttpResponse {
        val body = req.requireJsonBody<EmailRequestBody>()

        val success = emailsServer.sendEmail(
            recipientsCode = body.recipientsCode,
            subject = body.subject,
            text = body.text
        )

        return if (success) {
            SuccessResponse.SUCCESS
        } else {
            ServerErrorResponse(req.reqId)
        }
    }
}
