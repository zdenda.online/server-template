package online.zdenda.server.email.cfg

/**
 * Configuration of e-mails with recipients.
 */
internal data class EmailsCfg(
    val smtpHost: String,
    val smtpPort: Int,
    val smtpUser: String,
    val smtpPassword: String,
    val recipients: List<EmailRecipientsCfg>
) {

    companion object {
        fun load(cfg: Map<String, *>): EmailsCfg {
            val emails = cfg["emails"] as Map<*, *>
            return EmailsCfg(
                smtpHost = emails["smtp_host"].toString(),
                smtpPort = emails["smtp_port"].toString().toInt(),
                smtpUser = emails["smtp_user"].toString(),
                smtpPassword = emails["smtp_password"].toString(),
                recipients = (emails["recipients"] as List<*>).map { EmailRecipientsCfg.load(it as Map<*, *>) }
            )
        }
    }
}
