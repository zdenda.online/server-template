package online.zdenda.server.email

import online.zdenda.server.Module
import online.zdenda.server.email.cfg.EmailsCfg
import online.zdenda.server.email.domain.EmailServer
import online.zdenda.server.email.handler.EmailsHandler
import online.zdenda.server.http.handler.HttpHandler

/**
 * See README.md in module root directory for detailed information about this module.
 */
class EmailsModule : Module {

    override val name = "emails"

    override fun httpHandlers(cfg: Map<String, *>): List<HttpHandler> {
        val emailsCfg = EmailsCfg.load(cfg)
        val server = EmailServer(emailsCfg)
        return listOf(
            EmailsHandler(server)
        )
    }
}
