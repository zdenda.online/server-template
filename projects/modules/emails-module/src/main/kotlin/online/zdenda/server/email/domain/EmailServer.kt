package online.zdenda.server.email.domain

import online.zdenda.server.email.cfg.EmailsCfg
import online.zdenda.server.email.handler.EmailsHandler
import org.slf4j.LoggerFactory
import java.util.Date
import java.util.Properties
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

/**
 * E-mail server through which (e-mail) messages can be sent.
 */
internal class EmailServer(
    private val cfg: EmailsCfg
) {

    private lateinit var session: Session

    /**
     * Sends an e-mail to recipient identified by its code.
     *
     * @param recipientsCode code of recipients (to be translated to e-mail addresses)
     * @param subject subject of message
     * @param text text (body) of message
     * @return null if message was sent successfully, otherwise error message
     */
    fun sendEmail(recipientsCode: String, subject: String, text: String): Boolean {
        if (!this::session.isInitialized) session = prepareSession()

        val recipient = cfg.recipients.firstOrNull { it.code == recipientsCode }
        if (recipient == null) {
            val ex = IllegalArgumentException("Recipient code '${recipientsCode}' is not configured")
            logger.error(ex.message)
            throw ex
        }

        try {
            val mimeMessage = MimeMessage(session)
            mimeMessage.setFrom(InternetAddress(cfg.smtpUser))
            mimeMessage.setRecipients(
                Message.RecipientType.TO,
                InternetAddress.parse(recipient.addresses.joinToString(" "), false)
            )
            mimeMessage.subject = subject
            mimeMessage.setText(text, Charsets.UTF_8.name())
            mimeMessage.sentDate = Date()

            val smtpTransport = session.getTransport("smtp")
            smtpTransport.connect()
            smtpTransport.sendMessage(mimeMessage, mimeMessage.allRecipients)
            smtpTransport.close()
            return true
        } catch (ex: MessagingException) {
            logger.error("Failed to send an e-mail", ex)
            return false
        }
    }

    private fun prepareSession(): Session {
        val props = Properties()
        props["mail.smtp.host"] = cfg.smtpHost
        props["mail.smtp.port"] = cfg.smtpPort.toString()
        props["mail.smtp.auth"] = true
        props["mail.smtp.starttls.enable"] = true
        props["mail.smtp.ssl.protocols"] = "TLSv1.2"

        return Session.getDefaultInstance(props, object : javax.mail.Authenticator() {
            override fun getPasswordAuthentication(): PasswordAuthentication {
                return PasswordAuthentication(cfg.smtpUser, cfg.smtpPassword)
            }
        })
    }

    companion object {
        private val logger = LoggerFactory.getLogger(EmailsHandler::class.java)
    }
}
