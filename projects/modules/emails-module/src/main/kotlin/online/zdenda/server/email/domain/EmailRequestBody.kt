package online.zdenda.server.email.domain

/**
 * Request to send an e-mail to given recipient identified by its code.
 */
internal data class EmailRequestBody(
    val recipientsCode: String,
    val subject: String,
    val text: String
)
