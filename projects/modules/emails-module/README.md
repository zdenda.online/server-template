# Emails Module

E-mails module adds single public HTTP handler for sending a message via e-mail (configured by recipient code). For
security reasons, the clients do not send e-mail address directly in requests but send just recipient code that is
mapped to it. This ensures that e-mail addresses and not available in the browser nor in the HTTP communication. But
more importantly, clients cannot send e-mail message to any arbitrary mailbox but only to those that are configured.

The module is typically used if there is a public communication (e.g. contact forms) from web application users that
needs to delivered into mailbox of various people.

## HTTP Endpoints

See [OpenAPI documentation in HTML] or [OpenAPI documentation in YAML] that contains description of all endpoints of
this module.

## Configuration

See [configuration fragment] that contains description of all configuration properties of this module.

[configuration fragment]: ./assets/config-fragment-sample.yml

[OpenAPI documentation in YAML]: ./assets/emails-module-api.yml

[OpenAPI documentation in HTML]: ./assets/emails-module-api.html
