package online.zdenda.server.email

import online.zdenda.server.Module
import online.zdenda.server.email.cfg.ProxyCfg
import online.zdenda.server.email.handler.ProxyHandler
import online.zdenda.server.http.handler.HttpHandler

/**
 * See README.md in module root directory for detailed information about this module.
 */
class ProxyModule : Module {

    override val name = "proxy"

    override fun httpHandlers(cfg: Map<String, *>): List<HttpHandler> {
        val proxyCfg = ProxyCfg.load(cfg)
        return listOf(
            ProxyHandler(proxyCfg)
        )
    }
}
