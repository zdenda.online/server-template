package online.zdenda.server.email.handler

import online.zdenda.server.email.cfg.ProxyCfg
import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.response.HttpResponse
import org.slf4j.LoggerFactory
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

/**
 * Handler that just proxies given request to other server defined in request header (name is configured).
 */
internal class ProxyHandler(
    private val cfg: ProxyCfg
) : HttpHandler {

    override val methods = HttpMethod.values().filter { it != HttpMethod.OPTIONS }.toList()
    override val urlTemplate = "/public/proxy"
    override val authRoleName = null

    override fun handle(req: HttpRequest): HttpResponse {
        val proxyUrl = req.headers[cfg.urlHeader]
            ?: throw IllegalArgumentException("Missing '${cfg.urlHeader}' header in the request")

        val canProxy = cfg.allowedURLs.any { proxyUrl.startsWith(it) }
        if (!canProxy) {
            logger.error("Proxy URL '$proxyUrl' is not one of allowed URLS [${cfg.allowedURLs.joinToString(",")}]")
            throw IllegalArgumentException("Proxied URL is not on of allowed ones")
        }

        val url = URL(proxyUrl)
        with(url.openConnection() as HttpURLConnection) {
            doInput = true
            doOutput = (req.method != HttpMethod.GET && req.hasBody())
            requestMethod = req.method.toString()

            req.headers
                .filter { it.key != cfg.urlHeader } // do not re-send proxy header
                .forEach { setRequestProperty(it.key, it.value) }

            if (doOutput && req.hasBody()) outputStream.write(req.body!!) // pipe request body to proxied request body

            val headers = headerFields // do not proxy CORS headers (may differ from what browser sent)
                .filter { it.key != null && it.value.isNotEmpty() && !it.key.startsWith("Access-Control-") }
                .map { it.key to it.value.first() }
                .toMap()
            try {
                val respBytes = readStream(inputStream, true)
                return HttpResponse(responseCode, headers, respBytes)
            } catch (e: FileNotFoundException) { // funny JVM API throws this on 404
                val respBytes = readStream(errorStream, false)
                return HttpResponse(404, headers, respBytes)
            } catch (e: IOException) {
                val respBytes = readStream(errorStream, false)
                return HttpResponse(responseCode, headers, respBytes)
            }
        }
    }

    private fun readStream(stream: InputStream?, rethrowException: Boolean): ByteArray {
        return try {
            stream?.readBytes() ?: ByteArray(0)
        } catch (e: IOException) {
            logger.warn("Proxy stream cannot be read", e)
            if (rethrowException) {
                throw e
            } else {
                ByteArray(0)
            }
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ProxyHandler::class.java)
    }
}
