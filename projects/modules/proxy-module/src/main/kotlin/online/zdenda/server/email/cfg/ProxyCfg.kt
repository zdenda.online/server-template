package online.zdenda.server.email.cfg

/**
 * Configuration of proxy with URLs to proxy.
 */
internal data class ProxyCfg(
    val urlHeader: String,
    val allowedURLs: List<String>
) {

    companion object {
        fun load(cfg: Map<String, *>): ProxyCfg {
            val proxy = cfg["proxy"] as Map<*, *>
            return ProxyCfg(
                urlHeader = proxy["target_url_header"].toString(),
                allowedURLs = (proxy["allowed_urls"] as List<*>).map { it.toString() }
            )
        }
    }
}
