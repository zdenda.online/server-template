# Proxy Module

Proxy module adds a single HTTP handler that handles all HTTP methods and any request and proxies them to URL specified
in HTTP request header (its name is configured).

The module is typically used if there are backend services (APIs) that does not support CORS but need to be invoked
from the FE. This case is usually solved (and recommended to solve) via HTTP servers' (like NGINX or similar) via their
proxy pass features. However, sometimes it would be extra burden or there are some cases when such proxy needs to be
customized. In such scenarios, this proxy module can be useful.

## HTTP Endpoints

See [OpenAPI documentation in HTML] or [OpenAPI documentation in YAML] that contains description of all endpoints of
this module.

## Configuration

See [configuration fragment] that contains description of all configuration properties of this module.

[configuration fragment]: ./assets/config-fragment-sample.yml

[OpenAPI documentation in YAML]: ./assets/proxy-module-api.yml

[OpenAPI documentation in HTML]: ./assets/proxy-module-api.html
