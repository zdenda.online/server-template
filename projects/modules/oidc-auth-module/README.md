# OIDC Auth Module

Open ID Connect (OIDC) Auth module adds authorization via configured OIDC server via standardized OAuth2
[token introspection endpoint]. It uses standard OAuth2 client identified by its `client_id` and `client_secret` pair
that is provided by (configured in) OIDC server.

This module allows to distinguish between multiple *realms* where each *realm* has its own introspection endpoint and
can serve for authorization of one or more roles. The first matching realm for given role is used for token check.

The module is typically used for authorization of requests by other modules. The HTTP server handlers define role that
is required for authorization and the server uses authorization mechanism provided by application configuration.

## Configuration

See [configuration fragment] that contains description of all configuration properties of this module.

[configuration fragment]: ./assets/config-fragment-sample.yml

[token introspection endpoint]: https://www.oauth.com/oauth2-servers/token-introspection-endpoint/

