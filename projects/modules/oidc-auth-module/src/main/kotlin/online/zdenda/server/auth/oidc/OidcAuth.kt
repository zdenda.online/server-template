package online.zdenda.server.auth.oidc

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import online.zdenda.server.auth.Auth
import online.zdenda.server.auth.AuthInfo
import online.zdenda.server.auth.oidc.cfg.OidcAuthCfg
import online.zdenda.server.http.httpPost
import online.zdenda.server.http.urlEncode
import org.slf4j.LoggerFactory
import java.util.Base64

/**
 * Auth via OpenID Connect server using configured introspection endpoints for given auth roles.
 */
internal class OidcAuth(
    private val cfg: OidcAuthCfg
) : Auth {

    override fun asRole(roleName: String, token: String): AuthInfo {
        val realm = cfg.realms.firstOrNull { it.authRoles.contains(roleName) }
            ?: throw IllegalStateException("Misconfiguration? No matching realm for role $roleName")

        val authHeader = authHeader(realm.clientId, realm.clientSecret)
        val userId = auth(realm.introspectionEndpoint, authHeader, token)
        return if (userId != null) AuthInfo(userId) else AuthInfo.UNAUTHORIZED
    }


    private fun auth(introspectionEndpoint: String, authHeader: String, token: String): String? {
        val body = ("token_type_hint=access_token&token=${token.urlEncode()}")
            .toByteArray(Charsets.UTF_8)
        val headers = mapOf(
            "Authorization" to authHeader,
            "Content-Type" to "application/x-www-form-urlencoded",
            "Content-Length" to body.size.toString()
        )

        val response = introspectionEndpoint.httpPost(body, headers)
        if (!response.isSuccessful()) {
            logger.error("Failed to introspect token from endpoint: '$introspectionEndpoint'")
            logger.error(response.toString())
            return null
        }
        val deserializedResponse = jsonObjectMapper.readValue(response.body, OidcIntrospectionResponse::class.java)

        return if (deserializedResponse.active) {
            val user = parseJwtPayload(token).sub

            if (deserializedResponse.exp == null || (System.currentTimeMillis() / 1000) > deserializedResponse.exp) {
                logger.warn("Attempt to authorize as $user via token that is expired")
                return null
            }

            logger.debug("Successfully authorized as '$user'")
            parseJwtPayload(token).sub
        } else {
            logger.warn("Attempt to authorize via token that is not active")
            null
        }
    }

    private fun authHeader(clientId: String, clientSecret: String): String {
        val rawCredentials = "$clientId:$clientSecret".toByteArray(Charsets.UTF_8)
        val encodedCredentials = base64Encoder.encodeToString(rawCredentials)
        return "Basic $encodedCredentials"
    }

    private fun parseJwtPayload(token: String): OidcJwtPayload {
        val parts = token.split(".")
        val rawPayload = base64UrlDecoder.decode(parts[1].toByteArray(Charsets.UTF_8))
        return jsonObjectMapper.readValue(rawPayload, OidcJwtPayload::class.java)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(OidcAuthCfg::class.java)
        private val jsonObjectMapper = jacksonObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        private val base64Encoder = Base64.getEncoder()
        private val base64UrlDecoder = Base64.getUrlDecoder()
    }

    private data class OidcIntrospectionResponse(
        val active: Boolean,
        val exp: Long?
    )

    private data class OidcJwtPayload(
        val sub: String
    )
}
