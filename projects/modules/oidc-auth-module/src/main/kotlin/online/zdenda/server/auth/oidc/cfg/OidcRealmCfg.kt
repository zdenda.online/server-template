package online.zdenda.server.auth.oidc.cfg

/**
 * OpenID Connect realm that is used for authorization of requests that are for given auth roles.
 */
internal data class OidcRealmCfg(
    val authRoles: List<String>,
    val introspectionEndpoint: String,
    val clientId: String,
    val clientSecret: String
) {

    companion object {
        fun load(cfg: Map<*, *>): OidcRealmCfg {
            return OidcRealmCfg(
                authRoles = (cfg["role_names"] as List<*>).map { it.toString() },
                introspectionEndpoint = cfg["introspection_endpoint"].toString(),
                clientId = cfg["client_id"].toString(),
                clientSecret = cfg["client_secret"].toString()
            )
        }
    }
}
