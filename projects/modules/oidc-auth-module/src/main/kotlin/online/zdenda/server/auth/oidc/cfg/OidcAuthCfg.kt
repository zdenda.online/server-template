package online.zdenda.server.auth.oidc.cfg

/**
 * OpenID Connect realms that is used for authorization of requests.
 */
internal data class OidcAuthCfg(
    val realms: List<OidcRealmCfg>,
) {

    companion object {
        fun load(cfg: Map<*, *>): OidcAuthCfg {
            val oidcServer = cfg["oidc_auth"] as Map<*, *>

            return OidcAuthCfg(
                realms = (oidcServer["realms"] as List<*>).map { OidcRealmCfg.load(it as Map<*, *>) },
            )
        }
    }
}
