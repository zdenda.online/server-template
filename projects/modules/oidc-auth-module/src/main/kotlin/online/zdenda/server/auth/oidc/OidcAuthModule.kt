package online.zdenda.server.auth.oidc

import online.zdenda.server.Module
import online.zdenda.server.auth.Auth
import online.zdenda.server.auth.oidc.cfg.OidcAuthCfg

/**
 * See README.md in module root directory for detailed information about this module.
 */
class OidcAuthModule : Module {

    override val name: String = "oidc-auth"

    override fun auth(cfg: Map<String, *>): Auth {
        val oidcCfg = OidcAuthCfg.load(cfg)
        return OidcAuth(oidcCfg)
    }
}
