# File-system Assets Storage Module

Implementation of [Assets Storage Module] that uses file-system for storing assets.
See [Assets Storage Module README] for more information about endpoints and semantics of storage.

## Configuration

See [configuration fragment] that contains description of all configuration properties of this module.

[Assets Storage Module]: ../assets-storage-module

[Assets Storage Module README]: ../assets-storage-module/README.md

[configuration fragment]: ./assets/config-fragment-sample.yml

