package online.zdenda.server.assets

import online.zdenda.server.assets.domain.AssetsStorage
import online.zdenda.server.assets.domain.FileSystemAssetsStorage
import online.zdenda.server.assets.domain.cfg.FsAssetsStorageCfg

/**
 * See README.md in module root directory for detailed information about this module.
 */
class FileSystemAssetsStorageModule : AssetsStorageModule() {

    override val name: String = "file-system-assets-storage"

    override fun createStorage(cfg: Map<String, *>): AssetsStorage {
        val fsCfg = FsAssetsStorageCfg.load(cfg)
        if (!fsCfg.directory.canWrite())
            throw IllegalArgumentException("Missing +w permission for assets storage directory: '${fsCfg.directory.path}'")
        return FileSystemAssetsStorage(fsCfg)
    }

    override fun editorRoleName(cfg: Map<String, *>): String {
        val fsCfg = FsAssetsStorageCfg.load(cfg)
        return fsCfg.editorRoleName
    }
}
