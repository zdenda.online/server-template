package online.zdenda.server.assets.domain

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import online.zdenda.server.assets.domain.cfg.FsAssetsStorageCfg
import java.io.File

/**
 * Storage that uses file-system for persistence of assets.
 *
 * It stores one file for every asset under directory with name of asset kind.
 * If asset has any indices, it stores one additional file for it.
 */
internal class FileSystemAssetsStorage(
    cfg: FsAssetsStorageCfg
) : AssetsStorage {

    private val rootDir = cfg.directory

    override fun save(kind: AssetKind, id: AssetId, asset: ByteArray, indices: List<StorageIndex>) {
        val kindDir = kindDir(kind, true)
        if (indices.isNotEmpty()) {
            val idxContents = objectMapper.writeValueAsBytes(Indices(indices))
            idxFile(kindDir, id).writeBytes(idxContents)
        }
        assetFile(kindDir, id).writeBytes(asset)
    }

    override fun delete(kind: AssetKind, id: AssetId) {
        val kindDir = kindDir(kind, false)
        if (!kindDir.exists()) return // nothing to delete

        assetFile(kindDir, id).delete()
        idxFile(kindDir, id).delete()
    }

    override fun tryFindById(kind: AssetKind, id: AssetId): ByteArray? {
        val kindDir = kindDir(kind, false)
        if (!kindDir.exists()) return null

        val assetFile = assetFile(kindDir, id)
        return if (assetFile.exists()) assetFile.readBytes() else null
    }

    override fun findAll(
        kind: AssetKind,
        filterIndices: List<StorageIndex>,
        isDescending: Boolean,
        limit: Int
    ): List<ByteArray> {
        val kindDir = kindDir(kind, false)
        if (!kindDir.exists()) return emptyList()

        val allFiles = kindDir.listFiles() ?: emptyArray()
        var assetIds = allFiles.map { it.name }.filter { !it.endsWith(INDEX_EXTENSION) }.distinct()
        if (filterIndices.isNotEmpty()) assetIds = assetIds.filter { id ->
            val idxFile = idxFile(kindDir, id.toAssetId())
            if (idxFile.exists()) {
                val assetIndices = objectMapper.readValue(idxFile.readBytes(), Indices::class.java)
                assetIndices.matches(filterIndices)
            } else {
                false // filter is set but no index file present
            }
        }

        assetIds = assetIds.sortedBy { it }
        if (isDescending) assetIds = assetIds.reversed()
        if (limit > 0) assetIds = assetIds.take(limit)

        return assetIds.map { assetFile(kindDir, it.toAssetId()).readBytes() }
    }

    private fun kindDir(kind: AssetKind, mkdirs: Boolean): File {
        val dir = File(rootDir, kind.name)
        if (mkdirs) dir.mkdirs()
        return dir
    }

    private fun assetFile(kindDir: File, id: AssetId): File {
        return File(kindDir, id.id)
    }

    private fun idxFile(kindDir: File, id: AssetId): File {
        return File(kindDir, "${id.id}${INDEX_EXTENSION}")
    }

    private data class Indices(val indices: List<StorageIndex>) {

        fun matches(filter: List<StorageIndex>): Boolean {
            return filter.all { filterIdx ->
                val foundIdx = indices.firstOrNull { it.name == filterIdx.name }
                foundIdx != null && foundIdx.value == filterIdx.value
            }
        }
    }

    companion object {
        private const val INDEX_EXTENSION = ".idx"
        private val objectMapper = jacksonObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
    }
}
