package online.zdenda.server.assets.domain.cfg

import java.io.File

/**
 * Configuration of file-system storage for assets.
 */
internal data class FsAssetsStorageCfg(
    val directory: File,
    val editorRoleName: String
) {

    companion object {
        fun load(cfg: Map<String, *>): FsAssetsStorageCfg {
            val proxy = cfg["assets_fs_storage"] as Map<*, *>
            return FsAssetsStorageCfg(
                directory = File(proxy["directory"].toString()),
                editorRoleName = proxy["editor_role_name"].toString()
            )
        }
    }
}
