dependencies {
    implementation(project(":core"))

    api(project(":assets-storage-module"))
}
