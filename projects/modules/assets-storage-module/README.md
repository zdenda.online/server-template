# Assets Storage Module

Assets storage module itself cannot be used registered to application but serves as a base module that can be extended
by specific storage implementation (and such can be registered to application). Implementors of this base module just
need to extend [AssetsStorageModule] that provide implementation of [AssetsStorage]. This base module takes care of the
rest that is mainly providing generic HTTP handlers (that are independent of used storage) and serialization of
payloads to be stored or retrieved to / from storage. Optionally, implementing modules can provide
custom [Serialization] implementations if built-in are not sufficient ([JSON] and [binary]).

The module is typically used for static assets that are public for retrieval by customers but require specific auth role
for modifications (save/update or delete). These can be news in the web-page, products in shop catalog or similar.

This module does NOT require or provide any schema, it is up to clients (e.g. web pages) to provide a semantic meaning
of the data. This module just stores the data as-is and returns them in a same way. The only logic it can provide is
distinguishing whether single or multiple assets are queried and optionally store index for each asset that can be used
for querying subset of assets (e.g. if asset represents article in JSON, client can add index for year and later fetch
only articles in given year).

In case of retrieval of multiple entities, it uses natural (alphabetical) ordering of asset IDs. Due to this, it is
recommended to use IDs in a smart way. E.g. for storing articles, it makes sense to use `YYYY-MM-dd-{name}` format or
similar as the clients may want to retrieve articles depending on when they were published.

## HTTP Endpoints

See [OpenAPI documentation in HTML] or [OpenAPI documentation in YAML] that contains description of all endpoints of
this module.

## Configuration

This module does not have any configuration but very likely its implementors do.

[AssetsStorageModule]: ./src/main/kotlin/online/zdenda/server/assets/AssetsStorageModule.kt

[AssetsStorage]: ./src/main/kotlin/online/zdenda/server/assets/domain/AssetsStorage.kt

[Serialization]: ./src/main/kotlin/online/zdenda/server/assets/domain/serialization/Serialization.kt

[JSON]: ./src/main/kotlin/online/zdenda/server/assets/domain/serialization/JsonJacksonSerialization.kt

[binary]: ./src/main/kotlin/online/zdenda/server/assets/domain/serialization/BinarySerialization.kt

[OpenAPI documentation in YAML]: ./assets/assets-storage-module-api.yml

[OpenAPI documentation in HTML]: ./assets/assets-storage-module-api.html
