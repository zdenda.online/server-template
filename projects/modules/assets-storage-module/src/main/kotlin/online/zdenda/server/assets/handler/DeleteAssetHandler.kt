package online.zdenda.server.assets.handler

import online.zdenda.server.assets.domain.AssetsStorage
import online.zdenda.server.assets.domain.toAssetId
import online.zdenda.server.assets.domain.toAssetKind
import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.response.HttpResponse
import online.zdenda.server.http.response.SuccessResponse

/**
 * Handler that just maps incoming HTTP request to [AssetsStorage.delete] domain call.
 */
internal class DeleteAssetHandler(
    private val storage: AssetsStorage,
    override val authRoleName: String
) : HttpHandler {

    override val methods = listOf(HttpMethod.DELETE)
    override val urlTemplate = "/operator/assets/{kind}/{asset-id}"

    override fun handle(req: HttpRequest): HttpResponse {
        val kind = req.requireParameter("kind").toAssetKind()
        val assetIdWithExtension = req.requireParameter("asset-id")
        val assetId = assetIdWithExtension.toAssetId()

        storage.delete(kind, assetId)
        return SuccessResponse.SUCCESS
    }
}
