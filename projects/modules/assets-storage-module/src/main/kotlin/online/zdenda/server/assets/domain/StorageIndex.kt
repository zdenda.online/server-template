package online.zdenda.server.assets.domain

/**
 * Represents a storage index for later searching assets.
 */
data class StorageIndex(
    val name: String,
    val value: String
) {

    companion object {

        /**
         * Creates storage indices from given parameter value.
         */
        fun fromParameter(paramValue: String?): List<StorageIndex> {
            return paramValue?.split(";")?.map {
                val pair = it.split(":")
                if (pair.size != 2) {
                    return@map null // will be filtered out afterwards
                } else {
                    return@map StorageIndex(pair[0], pair[1])
                }
            }?.filterNotNull() ?: emptyList()
        }
    }
}
