package online.zdenda.server.assets.domain.serialization

/**
 * Generic exception that may be thrown when serialization takes place.
 * It extends [IllegalArgumentException] as those are mapped as HTTP 400
 */
internal class SerializationException(cause: Throwable) : IllegalArgumentException(cause)
