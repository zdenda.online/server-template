package online.zdenda.server.assets.handler

import online.zdenda.server.assets.domain.AssetsStorage
import online.zdenda.server.assets.domain.StorageIndex
import online.zdenda.server.assets.domain.serialization.BinarySerialization
import online.zdenda.server.assets.domain.serialization.Serialization
import online.zdenda.server.assets.domain.toAssetKind
import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.response.HttpResponse

/**
 * Handler that just maps incoming HTTP request to [AssetsStorage.findAll] domain call
 * and serializes output via given [Serialization].
 */
class GetAssetsHandler(
    private val storage: AssetsStorage,
    private val serializations: Map<String, Serialization>
) : HttpHandler {

    override val methods = listOf(HttpMethod.GET)
    override val urlTemplate = "/public/assets/{kind}"
    override val authRoleName = null

    override fun handle(req: HttpRequest): HttpResponse {
        val kindWithExtension = req.requireParameter("kind")
        val kind = kindWithExtension.substringBeforeLast(".").toAssetKind()
        val format = kindWithExtension.substringAfterLast(".", "")
        val filter = StorageIndex.fromParameter(req.parameters["filter"])
        val isDescending = req.parameters["descending"]?.toBoolean() ?: false
        val limit = req.parameters["limit"]?.toInt() ?: 0

        val assets = storage.findAll(kind, filter, isDescending, limit)
        val serialization = serializations[format.lowercase()] ?: BinarySerialization()
        val body = serialization.toArrayResponse(assets)
        return HttpResponse(200, mapOf("Content-Type" to serialization.mimeType()), body)
    }
}
