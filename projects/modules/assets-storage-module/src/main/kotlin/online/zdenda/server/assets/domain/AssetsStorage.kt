package online.zdenda.server.assets.domain

/**
 * Storage for assets of various kinds.
 */
interface AssetsStorage {

    /**
     * Saves given asset to the storage, optionally with given indices.
     * If asset already exists, it gets overwritten.
     *
     * @param kind kind of asset to be saved
     * @param id ID of asset to be saved
     * @param asset asset content to be saved
     * @param indices optional indices to be stored for given asset
     */
    fun save(kind: AssetKind, id: AssetId, asset: ByteArray, indices: List<StorageIndex> = emptyList())

    /**
     * Deletes given asset from the storage (if exists, otherwise nothing happens).
     *
     * @param kind kind of asset to be deleted
     * @param id ID of asset to be deleted
     */
    fun delete(kind: AssetKind, id: AssetId)

    /**
     * Tries to find a single asset of given kind by its ID or null if not present in the storage.
     *
     * @param kind kind of asset to be searched for
     * @param id ID of asset to be searched for
     */
    fun tryFindById(kind: AssetKind, id: AssetId): ByteArray?

    /**
     * Finds all assets of given kind, optionally only those that have all given indices or with limit.
     *
     * @param kind kind of asset to be searched for
     * @param filterIndices indices to use for filtering
     * @param isDescending flag whether order should be descending (otherwise defaults to ascending order)
     * @param limit optional limit for how many assets to retrieve (defaults to 0 = no limit)
     * @return assets that match given criteria
     */
    fun findAll(
        kind: AssetKind,
        filterIndices: List<StorageIndex> = emptyList(),
        isDescending: Boolean = false,
        limit: Int = 0
    ): List<ByteArray>
}
