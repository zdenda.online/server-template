package online.zdenda.server.assets.handler

import online.zdenda.server.assets.domain.AssetsStorage
import online.zdenda.server.assets.domain.StorageIndex
import online.zdenda.server.assets.domain.serialization.BinarySerialization
import online.zdenda.server.assets.domain.serialization.Serialization
import online.zdenda.server.assets.domain.toAssetId
import online.zdenda.server.assets.domain.toAssetKind
import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.response.HttpResponse
import online.zdenda.server.http.response.SuccessResponse

/**
 * Handler that just maps incoming HTTP request to [AssetsStorage.save] domain call
 * and deserializes input via given [Serialization].
 */
class PutAssetHandler(
    private val storage: AssetsStorage,
    override val authRoleName: String,
    private val serializations: Map<String, Serialization>
) : HttpHandler {

    override val methods = listOf(HttpMethod.PUT)
    override val urlTemplate = "/operator/assets/{kind}/{asset-id}"

    override fun handle(req: HttpRequest): HttpResponse {
        val kind = req.requireParameter("kind").toAssetKind()
        val assetIdWithExtension = req.requireParameter("asset-id")
        val assetId = assetIdWithExtension.toAssetId()
        val format = assetIdWithExtension.substringAfterLast(".", "")

        val serialization = serializations[format.lowercase()] ?: BinarySerialization()
        val asset = serialization.fromSingleRequest(req.requireBody())
        storage.save(kind, assetId, asset, StorageIndex.fromParameter("indices"))
        return SuccessResponse.SUCCESS
    }
}
