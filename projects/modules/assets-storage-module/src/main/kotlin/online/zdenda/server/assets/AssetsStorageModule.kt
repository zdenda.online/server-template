package online.zdenda.server.assets

import online.zdenda.server.Module
import online.zdenda.server.assets.domain.AssetsStorage
import online.zdenda.server.assets.domain.serialization.BinarySerialization
import online.zdenda.server.assets.domain.serialization.JsonJacksonSerialization
import online.zdenda.server.assets.domain.serialization.Serialization
import online.zdenda.server.assets.handler.DeleteAssetHandler
import online.zdenda.server.assets.handler.GetAssetHandler
import online.zdenda.server.assets.handler.GetAssetsHandler
import online.zdenda.server.assets.handler.PutAssetHandler
import online.zdenda.server.http.handler.HttpHandler

/**
 * Abstract base class for module that provides specific implementation of storage so that this module can be used.
 * Implementing modules just need to implement [AssetsStorage] and extend this class.
 *
 * Optionally can add custom [Serialization] via [additionalSerializations] if desired.
 */
abstract class AssetsStorageModule : Module {

    /**
     * Creates a new storage implementation that will be used by module.
     *
     * @param cfg configuration
     * @return storage implementation
     */
    abstract fun createStorage(cfg: Map<String, *>): AssetsStorage

    /**
     * Role name for users that can save (edit) or delete assets.
     *
     * @return role name for editors
     */
    abstract fun editorRoleName(cfg: Map<String, *>): String

    /**
     * Clients may override this method to provide extra serialization implementations for the storage.
     * These will be override default ones in case of using same extension.
     *
     * @return name of extension to serialization to be used for this extension
     */
    open fun additionalSerializations(cfg: Map<String, *>): Map<String, Serialization> = emptyMap()

    override fun httpHandlers(cfg: Map<String, *>): List<HttpHandler> {
        val editorRoleName = editorRoleName(cfg)

        val serializations = DEFAULT_SERIALIZATIONS
        additionalSerializations(cfg).forEach { serializations[it.key] = it.value }

        val storage = createStorage(cfg)
        return listOf(
            GetAssetHandler(storage, serializations),
            GetAssetsHandler(storage, serializations),
            PutAssetHandler(storage, editorRoleName, serializations),
            DeleteAssetHandler(storage, editorRoleName),
        )
    }

    companion object {
        private val DEFAULT_SERIALIZATIONS = hashMapOf(
            "json" to JsonJacksonSerialization(),
            "jpg" to BinarySerialization("image/jpeg"),
            "jpeg" to BinarySerialization("image/jpeg"),
            "png" to BinarySerialization("image/png"),
            "webp" to BinarySerialization("image/webp"),
            "pdf" to BinarySerialization("application/pdf")
        )
    }
}
