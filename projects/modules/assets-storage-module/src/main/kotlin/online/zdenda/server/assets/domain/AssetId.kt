package online.zdenda.server.assets.domain

@JvmInline
value class AssetId(val id: String)

fun String.toAssetId() = AssetId(this)

