package online.zdenda.server.assets.domain

@JvmInline
value class AssetKind(val name: String)

fun String.toAssetKind() = AssetKind(this)

