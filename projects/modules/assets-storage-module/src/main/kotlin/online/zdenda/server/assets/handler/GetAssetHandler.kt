package online.zdenda.server.assets.handler

import online.zdenda.server.assets.domain.AssetsStorage
import online.zdenda.server.assets.domain.serialization.BinarySerialization
import online.zdenda.server.assets.domain.serialization.Serialization
import online.zdenda.server.assets.domain.toAssetId
import online.zdenda.server.assets.domain.toAssetKind
import online.zdenda.server.http.HttpMethod
import online.zdenda.server.http.HttpRequest
import online.zdenda.server.http.handler.HttpHandler
import online.zdenda.server.http.response.HttpResponse

/**
 * Handler that just maps incoming HTTP request to [AssetsStorage.tryFindById] domain call
 * and serializes output via given [Serialization].
 */
class GetAssetHandler(
    private val storage: AssetsStorage,
    private val serializations: Map<String, Serialization>
) : HttpHandler {

    override val methods = listOf(HttpMethod.GET)
    override val urlTemplate = "/public/assets/{kind}/{asset-id}"
    override val authRoleName = null

    override fun handle(req: HttpRequest): HttpResponse {
        val kind = req.requireParameter("kind").toAssetKind()
        val assetIdWithExtension = req.requireParameter("asset-id")
        val assetId = assetIdWithExtension.toAssetId()
        val format = assetIdWithExtension.substringAfterLast(".", "")

        val asset = storage.tryFindById(kind, assetId)
            ?: throw IllegalArgumentException("Asset with kind '${kind.name}' and ID '${assetId.id}' not found")
        val serialization = serializations[format.lowercase()] ?: BinarySerialization()
        val body = serialization.toSingleResponse(asset)
        return HttpResponse(200, mapOf("Content-Type" to serialization.mimeType()), body)
    }
}
