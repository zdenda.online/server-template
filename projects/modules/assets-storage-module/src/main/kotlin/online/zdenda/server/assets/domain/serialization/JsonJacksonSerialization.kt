package online.zdenda.server.assets.domain.serialization

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

/**
 * Serialization that uses Jackson (FasterXML) library for JSON serialization.
 */
internal class JsonJacksonSerialization : Serialization {

    private val objectMapper = newObjectMapper()

    override fun mimeType(): String {
        return "application/json"
    }

    override fun toSingleResponse(asset: ByteArray): ByteArray {
        // deserialize and serialize to verify the structure (it is valid JSON) and format it
        try {
            val jsonNode = objectMapper.readTree(asset)
            return objectMapper.writeValueAsBytes(jsonNode)
        } catch (e: JsonProcessingException) {
            throw SerializationException(e)
        }
    }

    override fun toArrayResponse(assets: List<ByteArray>): ByteArray {
        try {
            val wrapperObject = objectMapper.createObjectNode()
            val itemsArray = wrapperObject.putArray("items")
            assets.map { objectMapper.readTree(it) }
                .forEach { itemsArray.add(it) }
            return objectMapper.writeValueAsBytes(wrapperObject)
        } catch (e: JsonProcessingException) {
            throw SerializationException(e)
        }
    }

    override fun fromSingleRequest(asset: ByteArray): ByteArray {
        // deserialize and serialize to verify the structure (it is valid JSON) and format it
        try {
            val jsonNode = objectMapper.readTree(asset)
            return objectMapper.writeValueAsBytes(jsonNode)
        } catch (e: JsonProcessingException) {
            throw SerializationException(e)
        }

    }

    private fun newObjectMapper(): ObjectMapper {
        val objectMapper = jacksonObjectMapper()
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        return objectMapper
    }
}
