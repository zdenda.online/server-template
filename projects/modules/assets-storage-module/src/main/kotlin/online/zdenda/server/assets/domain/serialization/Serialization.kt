package online.zdenda.server.assets.domain.serialization

/**
 * Serialization is responsible for transforming input bytes to output bytes of assets and vice versa.
 */
interface Serialization {

    /**
     * Gets a MIME type that assets using this serialization represent (e.g. application/json).
     */
    fun mimeType(): String

    /**
     * Validates and formats single asset to format for response.
     */
    @Throws(SerializationException::class)
    fun toSingleResponse(asset: ByteArray): ByteArray

    /**
     * Validates, formats and converts list of assets to single response.
     */
    @Throws(SerializationException::class)
    fun toArrayResponse(assets: List<ByteArray>): ByteArray

    /**
     * Validates, formats and converts single input asset to single output asset.
     */
    @Throws(SerializationException::class)
    fun fromSingleRequest(asset: ByteArray): ByteArray
}
