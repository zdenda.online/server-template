package online.zdenda.server.assets.domain.serialization

/**
 * Serialization that uses original binary format or concat bytes in case of arrays.
 */
internal class BinarySerialization(
    private val mimeType: String = "application/octet-stream"
) : Serialization {

    override fun mimeType(): String {
        return mimeType
    }

    override fun toSingleResponse(asset: ByteArray): ByteArray {
        return asset
    }

    override fun toArrayResponse(assets: List<ByteArray>): ByteArray {
        if (assets.isEmpty()) return byteArrayOf()
        return assets.reduce { total, item -> total.plus(item) }
    }

    override fun fromSingleRequest(asset: ByteArray): ByteArray {
        return asset
    }

}
