plugins {
    `java-platform`
}

javaPlatform.allowDependencies()

val undertowVersion by extra("2.2.22.Final")
val logbackVersion by extra("1.2.11")
val log4jOverSlf4jVersion by extra("1.7.30")
val snakeyamlVersion by extra("1.30")
val jacksonVersion by extra("2.13.3")
val javaxMailVersion by extra("1.5.0-b01")
val junitVersion by extra("5.8.2")
val koTestVersion by extra("5.5.4")

dependencies {
    constraints {
        api("io.undertow:undertow-core:$undertowVersion")
        api("org.yaml:snakeyaml:$snakeyamlVersion")
        api("javax.mail:mail:$javaxMailVersion")

        api("ch.qos.logback:logback-classic:$logbackVersion")
        api("org.slf4j:log4j-over-slf4j:$log4jOverSlf4jVersion") // ignore missing Log4J appender

        api("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")
        api("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")
        api("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacksonVersion")

        api("org.junit.jupiter:junit-jupiter-api:$junitVersion")
        api("org.junit.jupiter:junit-jupiter-params:$junitVersion")
        api("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

        api("io.kotest:kotest-runner-junit5-jvm:$koTestVersion")
        api("io.kotest:kotest-assertions-core-jvm:$koTestVersion")
        api("io.kotest:kotest-property-jvm:$koTestVersion")
    }
}
