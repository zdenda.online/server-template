rootProject.name = "custom-server-name"

fileTree(
    mapOf("dir" to "projects", "include" to listOf("**/build.gradle.kts"))
).forEach { file ->
    include(file.parentFile.name)
    project(":${file.parentFile.name}").projectDir = file.parentFile
}

buildscript {
    repositories {
        mavenLocal()
        gradlePluginPortal()
        mavenCentral()
    }
}
