import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.21"
}

val nonKotlinProjects = listOf("platform")

allprojects {
    group = "online.zdenda.server"
    version = "0.0.1"

    configurations.all {
        resolutionStrategy {
            preferProjectModules()
        }
    }

    if (!nonKotlinProjects.contains(project.name)) {
        apply(plugin = "kotlin")

        repositories {
            mavenLocal()
            mavenCentral()
        }

        dependencies {
            api(platform(project(":platform")))
            implementation(kotlin("stdlib-jdk8"))
        }

        val compileKotlin: KotlinCompile by tasks
        compileKotlin.kotlinOptions {
            jvmTarget = "17"
        }

        kotlin {
            jvmToolchain(17)
        }

        tasks {
            test {
                useJUnitPlatform()
            }
        }
    }

    /**
     * (Re)generates OpenAPI documentation HTML from YML source.
     */
    tasks.register("generate-openapi-html") {
        doLast {
            // Find all API documentation files in this project
            val openApiYmlFiles = project.file("assets")
                .takeIf { it.exists() }
                ?.listFiles()
                ?.filter { it.name.endsWith("api.yml") } ?: ArrayList()

            openApiYmlFiles.forEach { openApiFile ->
                println("Generating OpenAPI HTML from'${openApiFile.name}'")
                exec {
                    workingDir = openApiFile.parentFile
                    executable = "redoc-cli"
                    args = listOf(
                        "bundle",
                        openApiFile.name,
                        "-o", openApiFile.name.replace(".yml", ".html"),
                        "--options.theme.colors.primary.main=#0e1d54"
                    )
                }
            }
        }
    }

    /**
     * Copies OpenAPI documentation files into /docs subdirectory of all applications that use the module.
     */
    tasks.register("copy-openapi-docs") {
        doLast {
            // Find all API documentation files in this project
            val openApiFiles = project.file("assets")
                .takeIf { it.exists() }
                ?.listFiles()
                ?.filter { it.name.endsWith("api.yml") || it.name.endsWith("api.html") } ?: ArrayList()

            // Find app projects that reference this module
            val targetDirs = File(rootProject.file("projects"), "apps")
                .takeIf { it.exists() }
                ?.listFiles()
                ?.filter { it.isDirectory && File(it, "docs").exists() }
                ?.filter { appProj ->
                    val buildFile = File(appProj, "build.gradle.kts")
                    buildFile.exists() && buildFile.readText(Charsets.UTF_8).contains(project.name)
                }
                ?.map { File(it, "docs") } ?: ArrayList()

            openApiFiles.forEach { openApiFile ->
                targetDirs.forEach { targetDir ->
                    println("Copying '${openApiFile.name}' to '${targetDir.parentFile.name}' app")
                    openApiFile.copyTo(File(targetDir, openApiFile.name), overwrite = true)
                }
            }
        }
    }

    /**
     * Generate config.yml example from all modules' configuration fragments that are used if the module is application
     */
    tasks.register("generate-app-config") {
        doLast {
            val configDir = project.file("config")
            if (projectDir.parentFile.name != "apps") return@doLast // project is not an app
            configDir.mkdirs() // create config directory if missing

            println("Generating config file for app ${project.name}")
            val targetConfig = StringBuilder()
            rootProject.file("projects").walk()
                .filter { it.name == "config-fragment-sample.yml" }
                .sorted()
                .forEach { configFragment ->
                    val moduleDirName = configFragment.parentFile.parentFile.name
                    if (buildFile.readText(Charsets.UTF_8).contains(moduleDirName)) {
                        println("Adding fragment from '$moduleDirName' module")
                        targetConfig.append(configFragment.readText(Charsets.UTF_8))
                        targetConfig.append("\n")
                    }
                }

            if (targetConfig.isNotBlank()) {
                File(configDir, "config.yml").writeText(targetConfig.toString(), Charsets.UTF_8)
            }
        }
    }
}

/**
 * Copies this project to new location renaming it as desired.
 */
tasks.register("to-new-project") {
    doLast {
        println("Preparing a new project from this template")
        println("Enter (technical) project name (e.g. 'my-new-sever'):")
        val targetTechName = readLine()?.trim()
        if (targetTechName.isNullOrBlank()) throw IllegalArgumentException("Invalid project name")
        val targetHumanName = targetTechName
            .split("-").joinToString(" ") { it.capitalize() }

        println("Enter project package (e.g. 'com.company.server'):")
        val targetPkg = readLine()?.trim()
        if (targetPkg.isNullOrBlank()) throw IllegalArgumentException("Invalid project package")
        val targetPkgDirs = targetPkg.split(".")

        println("Enter target directory:")
        val targetDir = File(readLine()?.trim())
        targetDir.mkdirs()

        val originalTechName = rootProject.name
        val originalUser = "${originalTechName}-user"
        val originalHumanName = originalTechName
            .split("-").joinToString(" ") { it.capitalize() }
        val origPkg = "online.zdenda.server"
        val origPkgDirs = origPkg.split(".")
        val filesCharset = Charsets.UTF_8
        println("Generating a new project with name '$targetTechName' (pkg: '${targetPkg}') to '${targetDir}'")

        println("Copying whole project to '$targetTechName'")
        projectDir.copyRecursively(targetDir, overwrite = true)
        val dirsToRemoveAfterCopy = listOf(".idea", ".gradle", ".git", "build")
        val filesToRemoveAfterCopy = listOf("config-local.yml")
        targetDir.walkTopDown().forEach { file ->
            if (file.isDirectory && dirsToRemoveAfterCopy.any { it == file.name }) {
                file.deleteRecursively()
            }
            if (file.isFile && filesToRemoveAfterCopy.any { it == file.name }) {
                file.delete()
            }
        }

        println("Replacing project name in files...")
        val filesToReplaceContent = listOf(".kt", ".kts", ".sh", ".service", ".yml", ".html", ".xml", ".md", ".txt")
        targetDir.walkTopDown().forEach { file ->
            if (file.isFile && filesToReplaceContent.any { file.name.endsWith(it) }) {
                println("... processing file ${file.name}")
                var fileContents = file.readText(filesCharset)
                fileContents = fileContents.replace(originalUser, targetTechName)
                fileContents = fileContents.replace(originalTechName, targetTechName)
                fileContents = fileContents.replace(originalHumanName, targetHumanName)
                fileContents = fileContents.replace(origPkg, targetPkg)
                file.writeText(fileContents, filesCharset)
            }
        }

        println("Renaming project name directories...")
        targetDir.walkTopDown().forEach { file ->
            if (file.isDirectory && file.name == originalTechName) {
                println("... renaming '${file.canonicalPath}'")
                file.renameTo(File(file.parentFile, targetTechName))
            }
        }

        println("Creating new packages...")
        targetDir.walkTopDown().forEach { file ->
            var origPkgLeaf = file
            origPkgDirs.forEach { origPkgLeaf = File(origPkgLeaf, it) }

            if (file.isDirectory && origPkgLeaf.exists()) {
                var targetPkgDir = file
                targetPkgDirs.forEach { targetPkgDir = File(targetPkgDir, it) }
                targetPkgDir.mkdirs()
                println("... creating new package '${targetPkgDir.canonicalPath}'")

                origPkgLeaf.copyRecursively(targetPkgDir, true)
                File(file, origPkgDirs.first()).deleteRecursively()
            }
        }

        println("Preparing root README...")
        File(targetDir, "README.md").delete() // delete template file
        File(targetDir, "README-project.md").renameTo(File(targetDir, "README.md")) // and replace with project one

        println("Project successfully copied to '${targetDir.canonicalPath}'")
    }
}
